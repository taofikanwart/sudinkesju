<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dashboard extends CI_Model {
	public function chart_capaiankinerja($id_ukpd, $id_pegawai,$awal, $akhir){
        $this->db->select('t_absensi.id_pegawai,m_waktukerja.bulan, m_waktukerja.jml_hari, aktifitas.bulan_aktifitas, aktifitas.capaian, prilaku.persen_prilaku, nilai, t_absensi.*');
        $this->db->from('m_waktukerja');

        $this->db->join('t_absensi', 'm_waktukerja.bulan = t_absensi.bulan AND t_absensi.id_pegawai ='.$id_pegawai.'', 'LEFT OUTER');

        $this->db->join('(SELECT id_pegawai,DATE_FORMAT(str_to_date(t_prilakukerja.bulan,"%Y%m"),"%Y-%m") as bln, (((orientasi_pelayanan+integrasi+komitmen+disiplin+kerjasama+kepemimpinan)/5)*0.1) as persen_prilaku FROM t_prilakukerja WHERE id_pegawai='.$id_pegawai.') AS prilaku', 'm_waktukerja.bulan = prilaku.bln','LEFT OUTER');

        $this->db->join('(SELECT sum(point) as capaian ,DATE_FORMAT(tanggal_aktifitas,"%Y-%m") as bulan_aktifitas FROM v_aktifitas WHERE id_pegawai = '.$id_pegawai.' GROUP BY  DATE_FORMAT(tanggal_aktifitas,"%Y-%m")) as aktifitas','m_waktukerja.bulan = aktifitas.bulan_aktifitas','LEFT OUTER');

        $this->db->join('m_penyerapan','m_waktukerja.bulan = m_penyerapan.bulan','LEFT OUTER');
    
        $this->db->where('m_waktukerja.`id_ukpd',$id_ukpd);
        $this->db->where('m_waktukerja.bulan <=', $akhir);
        $this->db->where('m_waktukerja.bulan >=', $awal);
        // $this->db->like('m_waktukerja.bulan',$tahun,'after');
        $this->db->group_by('m_waktukerja.bulan');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function totalpegawai(){
        $this->db->where('is_active', '1');
        $tpegawai = $this->db->from('m_pegawai')->count_all_results();
        return $tpegawai;
    }

    public function totalpegawaipns(){
        $this->db->where('is_active', '1');
        $this->db->where('status_pns', 'PNS');
        $tpegawai = $this->db->from('m_pegawai')->count_all_results();
        return $tpegawai;
    }

    public function totalpegawainon(){
        $this->db->where('is_active', '1');
        $this->db->where('status_pns', 'NON PNS');
        $tpegawai = $this->db->from('m_pegawai')->count_all_results();
        return $tpegawai;
    }
	

}

/* End of file Model_dashboard.php */
/* Location: ./application/models/Model_dashboard.php */
?>