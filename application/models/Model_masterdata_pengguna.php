<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_masterdata_pengguna extends CI_Model {
    var $table          = 'aauth_users';
    var $column_order   = array(null,'username','email','nama_sarana','banned','last_activity',null);
    var $column_search  = array('username','nama_sarana','email');
    var $order          = array('username' => 'ASC');

    public function _get_datatables_query($id_sarana,$jenis){
        $this->db->select('aauth_users.*,m_sarana.nama_sarana');
        $this->db->from($this->table);
        $this->db->join('m_sarana','m_sarana.id_sarana = aauth_users.id_sarana');
        if ($jenis != 'SUDIN') {
            $this->db->where('m_sarana.id_sarana',$id_sarana);
        }
        
        $i = 0;
        foreach ($this->column_search as $item){
            if($_POST['search']['value']){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i){
                    $this->db->group_end();
                }
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables($id_sarana, $jenis){
        $this->_get_datatables_query($id_sarana, $jenis);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered($id_sarana, $jenis){
        $this->_get_datatables_query($id_sarana, $jenis);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($id_sarana, $jenis){
        $this->db->select('aauth_users.*,m_sarana.nama_sarana');
        $this->db->from($this->table);
        $this->db->join('m_sarana','m_sarana.id_sarana = aauth_users.id_sarana');
        if ($jenis != 'sudin') {
            $this->db->where('m_sarana.id_sarana',$id_sarana);
        }

        return $this->db->count_all_results();
    }

    public function get_sarana_byid($id_sarana){  
        $query = $this->db->where('id_sarana', $id_sarana);
        $query = $this->db->get('m_sarana', 1, 0);

        if ($query->num_rows() <= 0){
			return FALSE;
		}
		return $query->row();
    }

    public function list_sarana(){
        $this->db->from('m_sarana');
        $query = $this->db->get();
        return $query->result();
    }

    public function list_group(){
        $this->db->from('aauth_groups');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_by_id($id){                
        $this->db->join('aauth_user_to_group', 'aauth_user_to_group.user_id = aauth_users.id');
        $this->db->join('aauth_groups','aauth_groups.id =  aauth_user_to_group.group_id');
        $this->db->where('aauth_users.id',$id);
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->row();
    }

    public function save($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
    
    public function delete_by_id($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
}

/* End of file Model_masterdata_pengguna.php */

?>