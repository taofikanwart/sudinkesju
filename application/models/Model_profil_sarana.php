<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_profil_sarana extends CI_Model {

    //PROFIL ==================================================
	public function get_by_id($id){   
        $this->db->select('m_sarana.id_sarana as idsarana, m_sarana.*, frm_profil.*');             
        $this->db->join('frm_profil','frm_profil.id_sarana =  m_sarana.id_sarana', 'left');
        $this->db->where('m_sarana.id_sarana',$id);
        $this->db->from('m_sarana');
        $query = $this->db->get();
        return $query->row();
    }

    public function cek_data($id,$table){
        $this->db->where('id_sarana',$id);
        $this->db->from($table);
        $query = $this->db->get();
        
        return $query->num_rows();
    }
	
    public function save($data, $table){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data, $table){
        $this->db->update($table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete($id,$table){
        $this->db->where('id', $id);
        $this->db->delete($table);
    }

    //PERIZINAN ==================================================
    public function get_perizinan($id){
        $this->db->select('m_sarana.id_sarana as idsarana, m_sarana.*, frm_perizinan.*');             
        $this->db->join('frm_perizinan','frm_perizinan.id_sarana =  m_sarana.id_sarana', 'left');
        $this->db->where('m_sarana.id_sarana',$id);
        $this->db->from('m_sarana');
        $query = $this->db->get();
        return $query->row();
    }

    //PELAYANAN ==================================================
    var $table_lay      = 'frm_layanan';
    var $column_order   = array(null,'layanan',null);
    var $column_search  = array('layanan');
    var $order          = array('layanan' => 'ASC');

    public function _get_datatables_query($id_sarana){
        $this->db->where('id_sarana', $id_sarana);
        $this->db->from($this->table_lay);

        
        $i = 0;
        foreach ($this->column_search as $item){
            if($_POST['search']['value']){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i){
                    $this->db->group_end();
                }
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables($id_sarana){
        $this->_get_datatables_query($id_sarana);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered($id_sarana){
        $this->_get_datatables_query($id_sarana);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($id_sarana){
        $this->db->where('id_sarana', $id_sarana);
        $this->db->from($this->table_lay);

        return $this->db->count_all_results();
    }

    //TEMPAT TIDUR ==================================================
    var $table_bed          = 'm_kelas';
    var $column_order_kls   = array(null,'nama_kelas',null);
    var $column_search_kls  = array('nama_kelas');
    var $order_kls          = array('id' => 'ASC');

    public function _get_datatables_bed(){
        $this->db->from($this->table_bed);
        
        $i = 0;
        foreach ($this->column_search_kls as $item){
            if($_POST['search']['value']){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search_kls) - 1 == $i){
                    $this->db->group_end();
                }
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order_kls[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_kls)){
            $order_kls = $this->order_kls;
            $this->db->order_by(key($order_kls), $order_kls[key($order_kls)]);
        }
    }

    public function get_datatables_bed(){
        $this->_get_datatables_bed();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered_bed(){
        $this->_get_datatables_bed();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_bed(){
        $this->db->from($this->table_bed);

        return $this->db->count_all_results();
    }

    public function get_jml_bed($id_sarana, $idkelas){
        $this->db->select('jumlah as jml');
        $this->db->where('id_sarana', $id_sarana);
        $this->db->where('kelas', $idkelas);
        $this->db->from('frm_tempattidur');
        $query = $this->db->get();

        if ($query->num_rows() <= 0){
            return FALSE;
        }
        return $query->row();
    }

    public function cek_data_bed($id,$kelas){
        $this->db->where('kelas', $kelas);
        $this->db->where('id_sarana',$id);
        $this->db->from('frm_tempattidur');
        $query = $this->db->get();

        if ($query->num_rows() <= 0){
            return FALSE;
        }
        return $query->row();
    }
}

/* End of file Model_profil_sarana.php */
/* Location: ./application/models/Model_profil_sarana.php */
?>