<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model  {
    var $table          = 'aauth_perms';

    function get_list_menu($group_id) {
        $this->db->select('aauth_perms.*, aauth_perm_to_group.*');
        $this->db->from('aauth_perms');
        $this->db->join('aauth_perm_to_group', 'aauth_perms.id = aauth_perm_to_group.perm_id', 'inner');
        $this->db->where('aauth_perm_to_group.group_id', $group_id);
        $res = $this->db->get();

    	if($res->num_rows()>0) {
            $list = $res->result_array();
            return $list;
    	}
    	return FALSE;
    }
    
    public function cari_sub_aktif($parent_id){
        $last       = $this->uri->total_segments();
        $record_num = $this->uri->segment($last);

        $this->db->where('parent_id', $parent_id);
        $this->db->like('url', $record_num, 'after');
        $sub = $this->db->get('aauth_perms');

        if ($sub->num_rows() > 0) {
            return TRUE;
        }
    }

    function get_pegawai_by_nip($nip){
        $query = $this->db->get_where('m_pegawai', array('nip' => $nip), 1, 0);
        
        return $query->row();
    }

    public function get_bagian_by_id($id){
        $query = $this->db->get_where('m_bagian', array('bagian_id' => $id), 1, 0);
        
        return $query->row();
    }

    function get_menu() {
        $this->db->from('aauth_perms');
        $res = $this->db->get();

        if($res->num_rows()>0) {
            $list = $res->result_array();
            return $list;
        }
        return FALSE;
    }

    function get_menu_parent() {
        $this->db->where('parent_id', 0);
        $this->db->from('aauth_perms');
        $res = $this->db->get()->result_array();
        return $res;
    }

    public function get_by_id($id){
        $this->db->from($this->table);
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $query->row();
    }

    public function save($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function get_user($user_id) {
        
        $this->db->where('id', $user_id);
        $this->db->join('m_sarana','m_sarana.id_sarana = aauth_users.id_sarana');
		$query = $this->db->get('aauth_users');

		if ($query->num_rows() <= 0){
			return FALSE;
		}
		return $query->row();
		
	}
}