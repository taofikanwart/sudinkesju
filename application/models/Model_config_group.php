<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_config_group extends CI_Model {
	var $table          = 'aauth_groups';
    var $column_order   = array(null,'name','definition',null);
    var $column_search  = array('name','definition');
    var $order          = array('id' => 'ASC');

    public function _get_datatables_query(){
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item){
            if($_POST['search']['value']){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i){
                    $this->db->group_end();
                }
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    ///////////////////////////////
    public function get_list_perm($parent = 0){
        $this->db->from('aauth_perms');
        $this->db->where('parent_id',$parent);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function get_group_by_id($id){
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row();
    }
    
    public function get_perm_by_group($group_id){
        $query = $this->db->get_where('aauth_perm_to_group', array('group_id' => $group_id));
        return $query->result();
    }
    
    public function delete_perm_by_group($group_id){
        $delete = $this->db->delete('aauth_perm_to_group', array('group_id' => $group_id));
        return $delete;
    }
	

}

/* End of file Model_config_group.php */
/* Location: ./application/models/Model_config_group.php */
?>