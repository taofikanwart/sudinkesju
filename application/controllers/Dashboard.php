<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    public function __construct() {
        parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('auth/login');
        }
        $this->load->model('Model_dashboard');

        $this->data['users']            = $this->aauth->get_user();
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['id_ukpd']          = $this->data['users']->id_ukpd;
        $this->data['pegawai']          = $this->Menu_model->get_pegawai_by_nip($this->data['users']->nip);
        $bagian                         = !empty($this->data['pegawai']) ? $this->data['pegawai']->bagian : "";
        $this->data['bagian']           = $this->Menu_model->get_bagian_by_id($bagian);
    }

    public function index() {
        $is_permit = $this->aauth->control_no_redirect('dashboard_view');
        if(!$is_permit) {
            redirect('no_permission');
        }

        $perms                      = "dashboard_view";
        $comments                   = "Dashboard View";
        $this->aauth->logit($perms, current_url(), $comments);
        
        $this->data['bc_parent']    = "Dashboard";
        $this->data['bc_child']     = "";
        $this->data['tpegawai']     = $this->Model_dashboard->totalpegawai();
        $this->data['tpegawaipns']  = $this->Model_dashboard->totalpegawaipns();
        $this->data['tpegawainon']  = $this->Model_dashboard->totalpegawainon();
        $this->load->view('view_dashboard', $this->data);
    }

    public function ajax_chart(){
        $tahun = date('Y'); 
        $bulan = date('m'); 
        if ($bulan < 12){
            $thn = $tahun - 1;
            $bln = sprintf("%02d", $bulan + 1);
        }else{
            $thn = $tahun;
            $bln = sprintf("%02d", 1);
        }
        $awal   = $thn."-".$bln;
        $akhir  = $tahun."-".$bulan;        

        $id_ukpd            = $this->data['id_ukpd'];
        $id_pegawai         = $this->data['pegawai']->id_pegawai;
        $lstcapaiankinerja  = $this->Model_dashboard->chart_capaiankinerja($id_ukpd, $id_pegawai, $awal, $akhir);

        $arrbln_awal = $bln-1;
        $arrbulan   = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $data       = array();
        $no         = 0;
        json_encode($lstcapaiankinerja);

        foreach ($lstcapaiankinerja as $kinerja) {
            $menit_kerja                = intval($kinerja['jml_hari']) * JML_MENIT_PER_HARI;
            $poin_capaian               = intval($kinerja['capaian']);
            
            $cuti_alasanpenting_k6      = intval($kinerja['cuti_alasanpenting_k6']) * JML_MENIT_PER_HARI;
            $cuti_tahunan               = intval($kinerja['cuti_tahunan']) * JML_MENIT_PER_HARI;
            $diklat                     = intval($kinerja['diklat']) * JML_MENIT_PER_HARI;
            $cuti_mendapingi_istri      = intval($kinerja['cuti_mendapingi_istri']) * CUTI_ALASAN_PENTING;
            $cutipersalinan1_2          = intval($kinerja['cutipersalinan1_2']) * CUTI_PERSALINAN;
            $cuti_sakit_plus            = intval($kinerja['cutisakit']) * CUTI_SAKIT_PLUS;
            $libur                      = intval($kinerja['libur']) * JML_MENIT_PER_HARI;

            $sakit                      = intval($kinerja['sakit']) * JML_MENIT_PER_HARI;
            $ijin                       = intval($kinerja['ijin']) * JML_MENIT_PER_HARI;
            $alpa                       = intval($kinerja['alpa']) * ALPA;
            $telat                      = intval($kinerja['telat']);
            $cutisakit                  = intval($kinerja['cutisakit']) * CUTI_SAKIT;
            $cutipentingl6              = intval($kinerja['cutipentingl6']) * JML_MENIT_PER_HARI;
            $ijinsthari                 = intval($kinerja['ijinsthari']) * IJIN_ST_HARI;
            $cutibersalin               = intval($kinerja['cutibersalin']) * JML_MENIT_PER_HARI;

            

            $total_penambahan   = $cuti_alasanpenting_k6 + $cuti_tahunan + $diklat + $cuti_mendapingi_istri + $cutipersalinan1_2 + $cuti_sakit_plus + $libur;
            $total_pengurangan  = $sakit + $ijin + $alpa + $telat + $cutisakit + $cutipentingl6 + $ijinsthari + $cutibersalin;
            
            $hasil_pengurangan  = $menit_kerja - $total_pengurangan;
            
            $hasil_akhircapaian = $poin_capaian + $total_penambahan;

            $hasil_akhirwaktu   = $hasil_pengurangan;
            $min_capaian_waktu  = min(intval($hasil_akhircapaian), intval($hasil_akhirwaktu));

            $nilai_aktifitas    = $poin_capaian == 0 && $hasil_akhircapaian == 0 ? 0 : ($min_capaian_waktu/$menit_kerja)*100; 
            $aktifitas70        = $nilai_aktifitas*0.7 > 70 ? 70 : $nilai_aktifitas*0.7;
            $perilaku10         = $kinerja['persen_prilaku'];
            $serapan            = $kinerja['nilai'] < 1 ? 0 : $kinerja['nilai'];
            $total              = floatval($aktifitas70)+floatval($perilaku10)+floatval($serapan);

            if ($arrbln_awal == 12) {
                $arrbln_awal    = 0;
                $thn            = $thn+1;
            }
            $data[] = array(
                'bulan' => $arrbulan[$arrbln_awal]." ".$thn,
                'nilai' => round($total,2),
            );
            $no++;
            $arrbln_awal++;
        }

        $output = array(
            "data"              => $data,
        );
        echo json_encode($data);
        
    }

}