<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {
	public function __construct() {
        parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('login');
        }
        $this->load->model('Model_absensi_bulanan');
        $this->load->model('Model_log');

        $this->data['users']            = $this->aauth->get_user();
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['id_ukpd']          = $this->data['users']->id_ukpd;
        $this->data['pegawai']          = $this->Menu_model->get_pegawai_by_nip($this->data['users']->nip);
        $bagian                         = !empty($this->data['pegawai']) ? $this->data['pegawai']->bagian : "";
        $this->data['bagian']           = $this->Menu_model->get_bagian_by_id($bagian);
    }

	public function index(){
		$is_permit = $this->aauth->control_no_redirect('Log_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $perms                      = "Log_page";
        $comments                   = "Log view";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->data['bc_parent']    = "Log";
        $this->data['bc_child']     = "Aktifitas";
    	$this->load->view('view_log', $this->data);
	}

	public function ajax_list(){
    	$filter_bln = $this->input->post('filter_bln',TRUE);
        $list       = $this->Model_log->get_datatables($filter_bln);
		$data 		= array();
		$no 		= $_POST['start'];
		foreach ($list as $list_array) {
			$no++;
			
            $row[]  = $list_array->nama_pegawai;
            $row[]  = $list_array->definition;
            $row[]  = $list_array->dateactivity;
            $row[]  = $list_array->page;
            $row[]  = $list_array->comments;
            $row[]  = $list_array->ipaddr;
			
			$data[] = $row;
		}

		$output = array(
			"draw" 				=> $_POST['draw'],
			"recordsTotal" 		=> $this->Model_log->count_all($filter_bln),
			"recordsFiltered" 	=> $this->Model_log->count_filtered($filter_bln),
			"data" 				=> $data,
		);
		echo json_encode($output);
    }

    public function ajax_clearlog(){
        $is_permit = $this->aauth->control_no_redirect('Log_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $filter_bln = $this->input->post('filter_bln',TRUE);
        $this->Model_log->clear_log($filter_bln);
        
        $perms      = "Log_page";
        $comments   = "Berhasil Clear Data Log";
        $this->aauth->logit($perms, current_url(), $comments);

        echo json_encode(array("status" => TRUE));
    }

}

/* End of file Log.php */
/* Location: ./application/controllers/Log.php */
?>