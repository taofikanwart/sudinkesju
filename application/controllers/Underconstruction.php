<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Underconstruction extends CI_Controller {
	 public function __construct() {
        parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('auth/login');
        }
        $this->load->model('Model_dashboard');

        $this->data['users']            = $this->aauth->get_user();
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    public function index(){
    	$this->data['bc_parent']    = "Underconstruction";
        $this->data['bc_child']     = "";
        $this->load->view('view_underconstruction', $this->data);
    }
}