<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {
	public function __construct(){
		parent::__construct();
		// $this->load->model('Model_blog');
	}

	public function index(){
		$data['title']			= 'Suku Dinas Kesehatan Jakarta Utara';
		$this->parser->parse('portal/index',$data);
	}

	public function kirimemail(){
		$config['protocol'] 	= 'smtp';
		$config['smtp_host'] 	= 'ssl://smtp.gmail.com';
		$config['smtp_port'] 	= 465;
		$config['smtp_user'] 	= 'pkcpademangan@gmail.com';
		$config['smtp_pass'] 	= 'pkcpademangan1';
		$config['charset']		= 'utf-8';
		$config['newline'] 		= "\r\n";
		$config['crlf'] 		= "\r\n";
		$config['wordwrap'] 	= TRUE;
		$config['mailtype']		= 'html';
		$this->email->initialize($config);

		//Data Email
		$emailfrom	= $this->input->post('email');
		$nama 		= $this->input->post('name');
		$emailto	= 'puskesmas.pademanga@jakarta.go.id';
		$emailbcc	= 'taofikanwar@gmail.com';
		$subject 	= $this->input->post('subject');
		$message	= $this->input->post('message');

		//Kirim email
		$this->email->from($emailfrom, $nama);
		$this->email->to($emailto);
		$this->email->cc("pkcpademangan@gmail.com");
		$this->email->bcc($emailbcc);
		$this->email->set_header('Header1', 'Value1');
		$this->email->subject("Email Dari : ".$emailfrom);
		$this->email->message($message);
		
		if (!$this->email->send()){
			show_error($this->email->print_debugger());
        	$pesan = 'Email Tidak Terkirim';
		}else{
			$pesan = 'Email Terkirim';	
		}

		echo $pesan;
	}

}