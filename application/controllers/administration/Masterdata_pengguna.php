<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterdata_pengguna extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('auth/login');
        }
        $this->load->model('Model_masterdata_pengguna');

        $this->data['users']            = $this->Menu_model->get_user($this->session->userdata('id'));
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['id_sarana']        = $this->data['users']->id_sarana;
        $this->data['sarana']           = $this->Model_masterdata_pengguna->get_sarana_byid($this->data['users']->id_sarana);
        
    }

	public function index()	{
		$is_permit = $this->aauth->control_no_redirect('masterdata_users_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $perms      = "masterdata_users_page";
        $comments   = "Data Pengguna";
        $this->aauth->logit($perms, current_url(), $comments);

        $this->data['bc_parent']    = "Master Data";
        $this->data['bc_child']     = "Pengguna";
    	$this->load->view('administration/view_data_pengguna', $this->data);
    }
    
    public function ajax_list(){
		$list 	= $this->Model_masterdata_pengguna->get_datatables($this->data['id_sarana'],$this->data['sarana']->jenis_sarana);
		$data 	= array();
		$no 	= $_POST['start'];
		foreach ($list as $list_array) {		

            if ($list_array->banned == 0) {
                $status = 'aktif';
                $click  = 'nonaktif';
                $icon   = 'fa fa-check-circle-o';
                $btn    = 'btn-success btn-xs';
            }else{
                $status = 'non aktif';
                $click  = 'aktifkan';
                $icon   = 'fa fa-times-circle-o';
                $btn    = 'btn-danger btn-xs';
            }

            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $list_array->username;
            $row[]  = $list_array->email;
            $row[]  = $list_array->nama_sarana;
            $row[]  = '<button class="btn '.$btn.'" title="'.$click.'" onclick="'.$click.'('."'".$list_array->id."'".')"><i class="glyphicon '.$icon.'"></i>'.$status.'</button>';;
            $row[]  = $list_array->last_activity;
			$row[] 	='
             <button type="button" class="btn btn-primary btn-xs" title="Sunting" onclick="edit('."'".$list_array->id."'".')"><i class="fa fa-edit"></i></button>
             <button type="button" class="btn btn-danger btn-xs" title="Hapus" onclick="remove('."'".$list_array->id."'".')"><i class="fa fa-trash"></i></button>';
			$data[] = $row;
		}

		$output = array(
			"draw" 				=> $_POST['draw'],
			"recordsTotal" 		=> $this->Model_masterdata_pengguna->count_all($this->data['id_sarana'],$this->data['sarana']),
			"recordsFiltered" 	=> $this->Model_masterdata_pengguna->count_filtered($this->data['id_sarana'],$this->data['sarana']),
			"data" 				=> $data,
		);
		echo json_encode($output);
	}

    public function ajax_get_by_id(){
        $is_permit = $this->aauth->control_no_redirect('masterdata_users_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $id         = $this->input->post('id',TRUE);
        $old_data   = $this->Model_masterdata_pengguna->get_by_id($id);
        
        if($old_data) {
            $res = array(
                'success'   => true,
                'messages'  => "Data found",
                'data'      => $old_data,
            );
        } else {
            $res = array(
                'success'   => false,
                'messages'  => "ID Tidak Ditemukan",
            );
        }
        echo json_encode($res);
    }

    public function ajax_add(){
        $is_permit = $this->aauth->control_no_redirect('masterdata_users_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $email           = $this->input->post('email',TRUE);
        $password        = $this->input->post('passwd',TRUE);
        $sarana          = $this->input->post('sarana',TRUE);
        $namalengkap     = $this->input->post('namalengkap',TRUE);
        $username        = $this->input->post('username',TRUE);
        $levelakses      = $this->input->post('levelakses',TRUE);

        $this->db->trans_begin();
        $insertuser = $this->aauth->create_user($email, $password, $sarana, $namalengkap, $username);
        
        if($insertuser){
            $this->aauth->add_member($insertuser, $levelakses);

        //     echo json_encode(array("status" => TRUE));  
        // }else{
        //     echo json_encode(array("status" => FALSE));
        }

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            echo json_encode(array("status" => FALSE));
        }else{
            echo json_encode(array("status" => TRUE));
        }
        

        $perms      = "masterdata_users_page";
        $comments   = "Tambah Master Data Pengguna";
        $this->aauth->logit($perms, current_url(), $comments);
    } 

    public function ajax_update(){
        $is_permit = $this->aauth->control_no_redirect('masterdata_users_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $id             = $this->input->post('id',TRUE);
        $namalengkap    = $this->input->post('namalengkap',TRUE);
        $sarana         = $this->input->post('sarana',TRUE);
        $email          = $this->input->post('email',TRUE) == "" ? FALSE : $this->input->post('email',TRUE);
        $password       = $this->input->post('passwd',TRUE) == "" ? FALSE : $this->input->post('passwd',TRUE);
        $username       = $this->input->post('username',TRUE);
        
        $levelakses     = $this->input->post('levelakses',TRUE);


        $this->db->trans_begin();
        $updateuser = $this->aauth->update_user($id, $namalengkap, $sarana, $email, $password, $username);
        if ($updateuser) {
            $this->aauth->remove_member_from_all($id);
            $update_passwd = $this->aauth->add_member($id, $levelakses);
        }    
        
        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            echo json_encode(array("status" => FALSE));
        }else{
            echo json_encode(array("status" => TRUE));
        }

        $perms      = "masterdata_users_page";
        $comments   = "Update Master Data Pengguna";
        $this->aauth->logit($perms, current_url(), $comments);
    }

    public function ajax_delete($id){
        $is_permit = $this->aauth->control_no_redirect('masterdata_users_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }
        
        $this->Model_masterdata_pengguna->delete_by_id($id);
        echo json_encode(array("status" => TRUE));

        $perms      = "masterdata_users_page";
        $comments   = "Hapus Master Data Pengguna";
        $this->aauth->logit($perms, current_url(), $comments);
    }
    
}

/* End of file Masterdata_pengguna.php */
/* Location: ./application/controllers/administration/Masterdata_pengguna.php */
?>