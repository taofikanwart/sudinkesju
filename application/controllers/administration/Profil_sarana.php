<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_sarana extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('auth/login');
        }
        $this->load->model('Model_profil_sarana');
        $this->load->model('Model_masterdata_pengguna');

        $this->data['users']            = $this->Menu_model->get_user($this->session->userdata('id'));
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['id_sarana']        = $this->data['users']->id_sarana;
    }

	public function index(){
		$is_permit = $this->aauth->control_no_redirect('sarana_profil_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $perms      = "sarana_profil_page";
        $comments   = "Group Management";
        $this->aauth->logit($perms, current_url(), $comments);

        $this->data['bc_parent']    = "Profil";
        $this->data['bc_child']     = "Sarana";
    	$this->load->view('administration/view_profil_sarana', $this->data);
	}

    //PROFIL ==================================================
    public function ajax_get_profil(){
        $is_permit = $this->aauth->control_no_redirect('sarana_profil_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $id         = $this->input->post('id',TRUE);
        $old_data   = $this->Model_profil_sarana->get_by_id($id);
        
        if($old_data) {
            $res = array(
                'success'   => true,
                'messages'  => "Data found",
                'data'      => $old_data,
            );
        } else {
            $res = array(
                'success'   => false,
                'messages'  => "ID Tidak Ditemukan",
            );
        }
        echo json_encode($res);
    }


    public function ajax_add(){
        $is_permit = $this->aauth->control_no_redirect('sarana_profil_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $id             = $this->input->post('id',TRUE);
        $data_ada       = $this->Model_profil_sarana->cek_data($id,'frm_profil');

        $data['id_sarana']      = $this->input->post('nama_fasyankes',TRUE);
        $data['direktur']       = $this->input->post('direktur_fasyankes',TRUE);
        
        $data['jenis']          = $this->input->post('jenis_fasyankes',TRUE);
        $data['kelas']          = $this->input->post('kelasrs_fasyankes',TRUE);
        $data['kepemilikan']    = $this->input->post('kepemilikan_fasyankes',TRUE);
        $data['penyelenggara']  = $this->input->post('penyelenggara_fasyankes',TRUE);
        $data['alamat']         = $this->input->post('alamat_fasyankes',TRUE);
        $data['kota']           = $this->input->post('kota_fasyankes',TRUE);
        $data['luas_tanah']     = $this->input->post('luas_tanah_fasyankes',TRUE);
        $data['luas_bangunan']  = $this->input->post('luas_bangunan_fasyankes',TRUE);
        $data['telp']           = $this->input->post('telp_fasyankes',TRUE);
        $data['email']          = $this->input->post('email_fasyankes',TRUE);
        $data['web']            = $this->input->post('web_fasyankes',TRUE);

        if ($data_ada > 0) {
            $proses = $this->Model_profil_sarana->update(array('id_sarana' => $id), $data,'frm_profil');

            $perms      = "sarana_profil_page";
            $comments   = "Update Master Data Profil";
        }else{
            $proses =  $this->Model_profil_sarana->save($data,'frm_profil');

            $perms      = "sarana_profil_page";
            $comments   = "Simpan Master Data Profil";

        }
        
        echo json_encode(array("status" => TRUE));
        $this->aauth->logit($perms, current_url(), $comments);
    } 

    //PERIZINAN ==================================================
    public function ajax_get_perizinan(){
        $is_permit = $this->aauth->control_no_redirect('sarana_profil_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $id         = $this->input->post('id',TRUE);
        $old_data   = $this->Model_profil_sarana->get_perizinan($id);
        
        if($old_data) {
            $res = array(
                'success'   => true,
                'messages'  => "Data found",
                'data'      => $old_data,
            );
        } else {
            $res = array(
                'success'   => false,
                'messages'  => "ID Tidak Ditemukan",
            );
        }
        echo json_encode($res);
    }

    public function ajax_add_perizinan(){
        $is_permit = $this->aauth->control_no_redirect('sarana_profil_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $id             = $this->input->post('id',TRUE);
        $data_ada       = $this->Model_profil_sarana->cek_data($id,'frm_perizinan');

        $data['id_sarana']          = $this->input->post('id',TRUE);
        $data['no_surat_izin']      = $this->input->post('no_surat_izin',TRUE);
        $data['masa_berlaku_izin']  = $this->input->post('masa_berlaku_izin',TRUE);
        $data['status_akre']        = $this->input->post('status_akre',TRUE);
        $data['no_sertifikat_akre'] = $this->input->post('no_sertifikat_akre',TRUE);
        $data['masa_berlaku_akre']  = $this->input->post('masa_berlaku_akre',TRUE);

        if ($data_ada > 0) {
            $proses = $this->Model_profil_sarana->update(array('id_sarana' => $id), $data, 'frm_perizinan');

            $perms      = "sarana_profil_page";
            $comments   = "Update Master Data Profil";
        }else{
            $proses =  $this->Model_profil_sarana->save($data,'frm_perizinan');

            $perms      = "sarana_profil_page";
            $comments   = "Simpan Master Data Profil";

        }
        
        echo json_encode(array("status" => TRUE));
        $this->aauth->logit($perms, current_url(), $comments);
    } 

    //LAYANAN ==================================================
    public function ajax_add_layanan(){
        $is_permit = $this->aauth->control_no_redirect('sarana_profil_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }


        $data['id_sarana']    = $this->input->post('id',TRUE);
        $data['layanan']      = $this->input->post('layanan',TRUE);

        $proses =  $this->Model_profil_sarana->save($data,'frm_layanan');

        if ($proses) {
            echo json_encode(array("status" => TRUE));
        }
        
        $perms      = "sarana_profil_page";
        $comments   = "Simpan Master Data Layanan";
        $this->aauth->logit($perms, current_url(), $comments);
    } 

    public function ajax_list_layanan(){
        $list   = $this->Model_profil_sarana->get_datatables($this->data['id_sarana']);
        $data   = array();
        $no     = $_POST['start'];
        foreach ($list as $list_array) { 
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $list_array->layanan;
            $row[]  ='<button type="button" class="btn btn-danger btn-xs" title="Hapus" onclick="remove_layanan('."'".$list_array->id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }

        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->Model_profil_sarana->count_all($this->data['id_sarana']),
            "recordsFiltered"   => $this->Model_profil_sarana->count_filtered($this->data['id_sarana']),
            "data"              => $data,
        );
        echo json_encode($output);
    }

    public function ajax_delete_layanan($id){
        $is_permit = $this->aauth->control_no_redirect('sarana_profil_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }
        
        $this->Model_profil_sarana->delete($id, 'frm_layanan');
        echo json_encode(array("status" => TRUE));

        $perms      = "sarana_profil_page";
        $comments   = "Hapus Profil Pelayanan";
        $this->aauth->logit($perms, current_url(), $comments);
    }

    //TEMPAT TIDUR ==================================================
    public function ajax_list_bed(){
        $list   = $this->Model_profil_sarana->get_datatables_bed();
        $data   = array();
        $no     = $_POST['start'];
        foreach ($list as $list_array) { 
            $no++;
            $val    = $this->Model_profil_sarana->get_jml_bed($this->data['id_sarana'], $list_array->id);
            $jumlah = $val == false ? '-' : $val->jml;

            $row    = array();
            $row[]  = $no;
            $row[]  = $list_array->nama_kelas;
            $row[]  = '<input type="text" class="form-control" id="'.$list_array->id.'" name="'.$list_array->id.'" placeholder="Jumlah Kamar" value="'.$jumlah.'" onchange="autosave('.$list_array->id.')">';
            $data[] = $row;
        }

        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->Model_profil_sarana->count_all_bed(),
            "recordsFiltered"   => $this->Model_profil_sarana->count_filtered_bed(),
            "data"              => $data,
        );
        echo json_encode($output);
    }

    public function ajax_autosave(){
        $is_permit = $this->aauth->control_no_redirect('sarana_profil_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $data['id_sarana']  = $this->data['id_sarana'];
        $data['kelas']      = $this->input->post('id_kelas',TRUE);
        $data['jumlah']     = $this->input->post('value',TRUE);

        $data_ada       = $this->Model_profil_sarana->cek_data_bed($data['id_sarana'], $data['kelas']);
        // echo $data_ada; die();

        if ($data_ada == false) {
            $proses =  $this->Model_profil_sarana->save($data,'frm_tempattidur');            
        }else{
            $proses = $this->Model_profil_sarana->update(array('id' => $data_ada->id), $data,'frm_tempattidur');
        }

        if ($proses) {
            echo json_encode(array("status" => TRUE));
        }
        
        $perms      = "sarana_profil_page";
        $comments   = "Simpan Data Tempat Tidur";
        $this->aauth->logit($perms, current_url(), $comments);
    }
}

/* End of file Profil_sarana.php */
/* Location: ./application/controllers/administration/Profil_sarana.php */
 ?>