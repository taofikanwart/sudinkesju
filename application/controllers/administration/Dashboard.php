<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('auth/login');
        }
        // $this->load->model('Model_dashboard');

        $this->data['users']            = $this->Menu_model->get_user($this->session->userdata('id'));
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['id_sarana']        = $this->data['users']->id_sarana;
    }

    public function index() {
        $is_permit = $this->aauth->control_no_redirect('administration_view');
        if(!$is_permit) {
            redirect('no_permission');
        }

        $perms                      = "administration_view";
        $comments                   = "Administration";
        $this->aauth->logit($perms, current_url(), $comments);
        
        $this->data['bc_parent']    = "Administration";
        $this->data['bc_child']     = "Dashboard";

        $this->load->view('administration/view_dashboard', $this->data);
    }

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/administration/Dashboard.php */
?>