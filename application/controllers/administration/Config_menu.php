<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_menu extends CI_Controller {
	public function __construct(){
		parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('auth/login');
        }

        $this->data['users']            = $this->Menu_model->get_user($this->session->userdata('id'));
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['id_sarana']        = $this->data['users']->id_sarana;
	}

	public function index(){

		$is_permit = $this->aauth->control_no_redirect('config_menus_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $perms      = "config_menus_page";
        $comments   = "Konfigurasi Menu";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->data['bc_parent']    = "Konfigurasi";
        $this->data['bc_child']     = "Menus";
        $this->data['list_menu_all']= $this->Menu_model->get_menu();
        $this->data['menu_parent']  = $this->Menu_model->get_menu_parent();
        
    	$this->load->view('administration/view_config_menus', $this->data);
		
	}

    public function ajax_get_by_id(){
        $id             = $this->input->post('id',TRUE);
        $old_data       = $this->Menu_model->get_by_id($id);
        
        if($old_data) {
            $res = array(
                'success'   => true,
                'messages'  => "Data found",
                'data'      => $old_data,
            );
        }
        echo json_encode($res);
    }

    public function ajax_add(){
        $data['name']           = $this->input->post('name');
        $data['definition']     = $this->input->post('definition');
        $data['parent_id']      = $this->input->post('parent_id');
        $data['url']            = $this->input->post('url');
        $data['icon']           = $this->input->post('icon') == ""? "fa fa-circle-o" : $this->input->post('icon');

        $this->Menu_model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update(){
        $data['name']           = $this->input->post('name');
        $data['definition']     = $this->input->post('definition');
        $data['parent_id']      = $this->input->post('parent_id');
        $data['url']            = $this->input->post('url');
        $data['icon']           = $this->input->post('icon') == ""? "fa fa-circle-o" : $this->input->post('icon');
        
        $this->Menu_model->update(array('id' => $this->input->post('id')), $data);
        
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id){
        $this->Menu_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }

}

/* End of file Config_menu.php */
/* Location: ./application/controllers/config/Config_menu.php */
?>