<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sdmk_list extends CI_Controller {
	public function __construct() {
        parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('auth/login');
        }
        $this->load->model('Model_masterdata_pengguna');
        $this->load->model('Model_sdmk_list');
        $this->load->helper('download');

        $this->data['users']            = $this->Menu_model->get_user($this->session->userdata('id'));
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['id_sarana']        = $this->data['users']->id_sarana;
        $this->data['sarana']           = $this->Model_masterdata_pengguna->get_sarana_byid($this->data['users']->id_sarana);
        
    }

	public function index()	{
		$is_permit = $this->aauth->control_no_redirect('sdmk_list_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $perms      = "sdmk_list_page";
        $comments   = "Data SDMK";
        $this->aauth->logit($perms, current_url(), $comments);

        $this->data['bc_parent']    = "SDMK";
        $this->data['bc_child']     = "List";
    	$this->load->view('administration/view_sdmk_list', $this->data);
    }

    public function download_format_import_sdmk(){
        force_download(base_url().'/assets/no-avatar.png', NULL);
    }

    public function import_sdmk(){
        $is_permit = $this->aauth->control_no_redirect('sdmk_list_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }
        
        $config['upload_path']      = 'data/file_excel_sdmk/';
        $config['allowed_types']    = 'xls|xlsx';
        $config['file_name']        = "data_sdmk".date('YmdHis');
        $this->load->library('upload', $config);
        if(!empty($_FILES['file_excel']['name'])){
            if (!$this->upload->do_upload('file_excel')){
                $error  = $this->upload->display_errors();
                $res    = array(
                    'success'       => false,
                    'messages'      => $error
                );
                
                // if permitted, do logit
                $perms      = "sdmk_list_page";
                $comments   = $error;
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $this->load->library('excel');
                $filedata   = $this->upload->data();
                $path       = 'data/file_excel_sdmk/'.$filedata['file_name'];
                //mulai insert data absensi
                $this->db->trans_begin();
                $objPHPExcel        = PHPExcel_IOFactory::load($path);
                $cell_collection    = $objPHPExcel->getActiveSheet()->getCellCollection();
                $arr_data           = array();
                foreach ($cell_collection as $cell) {
                    $column     = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row        = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                    if ($row <= 4) {
                        $header[$row][$column]      = $data_value;
                    } else {
                        $arr_data[$row][$column]    = $data_value;
                    }
                }
                //send the data in an array format
                $header = $header;
                $values = $arr_data;
                
                if(count($values) > 0){
                    $originalDate   = $this->input->post('bulan');
                    $bulan          = date("Y-m", strtotime($originalDate));

                    foreach($values as $val){                    
                        $data_sdmk_excel = array(
                            'id'                => '1',
                            'kota'              => 'KOTA JAKARTA UTARA',
                            'c1'                => $val['C'],
                            'c2'                => $val['D'],
                            'c3'                => $val['E'],
                            'c4'                => $val['F'],
                            'c5'                => $val['G'],
                            'c6'                => $val['H'],
                            'c7'                => $val['I'],
                            'c8'                => $val['J'],
                            'c9'                => $val['K'],
                            'c10'                => $val['L'],
                            'c11'                => $val['M'],
                            'c12'                => $val['N'],
                            'c13'                => $val['O'],
                            'c14'                => $val['P'],
                            'c15'                => $val['Q'],
                            'c16'                => $val['R'],
                            'c17'                => $val['S'],
                            'c18'                => $val['T'],
                            'c19'                => $val['U'],
                            'c20'                => $val['V'],
                            'c21'                => $val['W'],
                            'c22'                => $val['X'],
                            'c23'                => $val['Y'],
                            'c24'                => $val['Z'],
                            'c25'                => $val['AA'],
                            'c26'                => $val['AB'],
                            'c27'                => $val['AC'],
                            'c28'                => $val['AD'],
                            'c29'                => $val['AE'],
                            'c30'                => $val['AF'],
                            'c31'                => $val['AG'],
                            'c32'                => $val['AH'],
                            'c33'                => $val['AI'],
                            'c34'                => $val['AJ'],
                            'c35'                => $val['AK'],
                            'c36'                => $val['AL'],
                            'c37'                => $val['AM'],
                            'c38'                => $val['AN'],
                            'c39'                => $val['AO'],
                            'c40'                => $val['AP'],
                            'c41'                => $val['AQ'],
                            'c42'                => $val['AR'],
                            'c43'                => $val['AS'],
                            'c44'                => $val['AT'],
                            'c45'                => $val['AU'],
                            'c46'                => $val['AV'],
                            'c47'                => $val['AW'],
                            'c48'                => $val['AX'],
                            'c49'                => $val['AY'],
                            'c50'                => $val['AZ'],
                            'c51'                => $val['BA'],
                            'c52'                => $val['BB'],
                            'c53'                => $val['BC'],
                            'c54'                => $val['BD'],
                            'c55'                => $val['BE'],
                            'c56'                => $val['BF'],
                            'c57'                => $val['BG'],
                            'c58'                => $val['BH'],
                            'c59'                => $val['BI'],
                            'c60'                => $val['BJ'],
                            'c61'                => $val['BK'],
                            'c62'                => $val['BL'],
                            'c63'                => $val['BM'],
                            'c64'                => $val['BN'],
                            'c65'                => $val['BO'],
                            'c66'                => $val['BP'],
                            'c67'                => $val['BQ'],
                            'c68'                => $val['BR'],
                            'c69'                => $val['BS'],
                            'c70'                => $val['BT'],
                            'c71'                => $val['BU'],
                            'c72'                => $val['BV'],
                            'c73'                => $val['BW'],
                            'c74'                => $val['BX'],
                            'c75'                => $val['BY'],
                            'c76'                => $val['BZ'],
                            'c77'                => $val['CA'],
                            'c78'                => $val['CB'],
                            'c79'                => $val['CC'],
                            'c80'                => $val['CD'],
                            'c81'                => $val['CE'],
                            'c82'                => $val['CF'],
                            'c83'                => $val['CG'],
                            'c84'                => $val['CH'],
                            'c85'                => $val['CI'],
                            'c86'                => $val['CJ'],
                            'c87'                => $val['CK'],
                            'c88'                => $val['CL'],
                            'c89'                => $val['CM'],
                            'c90'                => $val['CN'],
                            'c91'                => $val['CO'],
                            'c92'                => $val['CP'],
                            'c93'                => $val['CQ'],
                            'c94'                => $val['CR'],
                            'c95'                => $val['CS'],
                            'c96'                => $val['CT'],

                        );
                        $this->Model_sdmk_list->insert_jumlah_sdmk($data_sdmk_excel);
                    }
                }

                if($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    $res = array(
                        'success'           => false,
                        'messages'          => 'Gagal upload file excel, hubungi web administrator.'
                    );
                    // if permitted, do logit
                    $perms      = "sdmk_list_page";
                    $comments   = "Gagal menyimpan data file excel ke database";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $this->db->trans_commit();
                    $res = array(
                        'success'       => true,
                        'messages'      => 'Import Excel Data Absensi Berhasil Diupload'
                    );
                    // if permitted, do logit
                    $perms      = "sdmk_list_page";
                    $comments   = "Berhasil Upload File Excel";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
        }else{
            $res = array(
                'success'       => false,
                'messages'      => 'Gagal, File Excel Tidak Diketahui'
            );
            // if permitted, do logit
            $perms      = "sdmk_list_page";
            $comments   = "Gagal, File Excel Tidak Diketahui";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function ajax_list(){
        $list   = $this->Model_sdmk_list->get_datatables();
        $data   = array();
        $no     = $_POST['start'];
        foreach ($list as $list_array) {
            $no++;

            $row    = array();
            $row[]  = $no;
            $row[]  = $list_array->kota;
            $row[]  = $list_array->c1;
            $row[]  = $list_array->c2;
            $row[]  = $list_array->c3;
            $row[]  = $list_array->c4;
            $row[]  = $list_array->c5;
            $row[]  = $list_array->c6;
            $row[]  = $list_array->c7;
            $row[]  = $list_array->c8;
            $row[]  = $list_array->c9;
            $row[]  = $list_array->c10;
            $row[]  = $list_array->c11;
            $row[]  = $list_array->c12;
            $row[]  = $list_array->c13;
            $row[]  = $list_array->c14;
            $row[]  = $list_array->c15;
            $row[]  = $list_array->c16;
            $row[]  = $list_array->c17;
            $row[]  = $list_array->c18;
            $row[]  = $list_array->c19;
            $row[]  = $list_array->c20;
            $row[]  = $list_array->c21;
            $row[]  = $list_array->c22;
            $row[]  = $list_array->c23;
            $row[]  = $list_array->c24;
            $row[]  = $list_array->c25;
            $row[]  = $list_array->c26;
            $row[]  = $list_array->c27;
            $row[]  = $list_array->c28;
            $row[]  = $list_array->c29;
            $row[]  = $list_array->c30;
            $row[]  = $list_array->c31;
            $row[]  = $list_array->c32;
            $row[]  = $list_array->c33;
            $row[]  = $list_array->c34;
            $row[]  = $list_array->c35;
            $row[]  = $list_array->c36;
            $row[]  = $list_array->c37;
            $row[]  = $list_array->c38;
            $row[]  = $list_array->c39;
            $row[]  = $list_array->c40;
            $row[]  = $list_array->c41;
            $row[]  = $list_array->c42;
            $row[]  = $list_array->c43;
            $row[]  = $list_array->c44;
            $row[]  = $list_array->c45;
            $row[]  = $list_array->c46;
            $row[]  = $list_array->c47;
            $row[]  = $list_array->c48;
            $row[]  = $list_array->c49;
            $row[]  = $list_array->c50;
            $row[]  = $list_array->c51;
            $row[]  = $list_array->c52;
            $row[]  = $list_array->c53;
            $row[]  = $list_array->c54;
            $row[]  = $list_array->c55;
            $row[]  = $list_array->c56;
            $row[]  = $list_array->c57;
            $row[]  = $list_array->c58;
            $row[]  = $list_array->c59;
            $row[]  = $list_array->c60;
            $row[]  = $list_array->c61;
            $row[]  = $list_array->c62;
            $row[]  = $list_array->c63;
            $row[]  = $list_array->c64;
            $row[]  = $list_array->c65;
            $row[]  = $list_array->c66;
            $row[]  = $list_array->c67;
            $row[]  = $list_array->c68;
            $row[]  = $list_array->c69;
            $row[]  = $list_array->c70;
            $row[]  = $list_array->c71;
            $row[]  = $list_array->c72;
            $row[]  = $list_array->c73;
            $row[]  = $list_array->c74;
            $row[]  = $list_array->c75;
            $row[]  = $list_array->c76;
            $row[]  = $list_array->c77;
            $row[]  = $list_array->c78;
            $row[]  = $list_array->c79;
            $row[]  = $list_array->c80;
            $row[]  = $list_array->c81;
            $row[]  = $list_array->c82;
            $row[]  = $list_array->c83;
            $row[]  = $list_array->c84;
            $row[]  = $list_array->c85;
            $row[]  = $list_array->c86;
            $row[]  = $list_array->c87;
            $row[]  = $list_array->c88;
            $row[]  = $list_array->c89;
            $row[]  = $list_array->c90;
            $row[]  = $list_array->c91;
            $row[]  = $list_array->c92;
            $row[]  = $list_array->c93;
            $row[]  = $list_array->c94;
            $row[]  = $list_array->c95;
            $row[]  = $list_array->c96;

            $data[] = $row;
        }

        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->Model_sdmk_list->count_all(),
            "recordsFiltered"   => $this->Model_sdmk_list->count_filtered(),
            "data"              => $data,
        );
        echo json_encode($output);
    }
}

/* End of file Sdmk_list.php */
/* Location: ./application/controllers/administration/Sdmk_list.php */
?>