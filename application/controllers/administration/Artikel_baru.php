<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel_baru extends CI_Controller {
	public function __construct() {
        parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('auth/login');
        }
        $this->load->model('Model_config_group');

        $this->data['users']            = $this->Menu_model->get_user($this->session->userdata('id'));
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['id_sarana']        = $this->data['users']->id_sarana;
    }

    public function index() {
        $is_permit = $this->aauth->control_no_redirect('artikel_baru_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $perms      = "artikel_baru_page";
        $comments   = "Artikel";
        $this->aauth->logit($perms, current_url(), $comments);

        $this->data['bc_parent']    = "Artikel";
        $this->data['bc_child']     = "Terbaru";
    	$this->load->view('administration/view_artikel_baru', $this->data);
    }

}

/* End of file Artikel_baru.php */
/* Location: ./application/controllers/administration/Artikel_baru.php */
?>