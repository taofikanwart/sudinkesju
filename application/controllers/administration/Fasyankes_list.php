<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fasyankes_list extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('auth/login');
        }
        $this->load->model('Model_fasyankes_list');
        $this->load->model('Model_masterdata_pengguna');        

        $this->data['users']            = $this->Menu_model->get_user($this->session->userdata('id'));
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['id_sarana']        = $this->data['users']->id_sarana;
        
    }

	public function index()	{
		$is_permit = $this->aauth->control_no_redirect('fasyankes_list_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $perms      = "fasyankes_list_page";
        $comments   = "Data Fasyankes";
        $this->aauth->logit($perms, current_url(), $comments);

        $this->data['bc_parent']    = "Fasyankes";
        $this->data['bc_child']     = "List";
    	$this->load->view('administration/view_fasyankes_list', $this->data);
    }

    public function ajax_list(){
		$list 	= $this->Model_fasyankes_list->get_datatables();
		$data 	= array();
		$no 	= $_POST['start'];
		foreach ($list as $list_array) {
			$no++;

            $row    = array();
            $row[]  = $no;
            $row[]  = $list_array->kode_sarana;
            $row[]  = $list_array->nama_sarana;
            $row[]  = $list_array->jenis_sarana;
            $row[]  = $list_array->kelas;
            $row[]  = $list_array->kepemilikan;
            $row[]  = $list_array->telp;
            $row[]  = $list_array->email;
			$row[] 	='
             <button type="button" class="btn btn-primary btn-xs" title="Lihat Detail" onclick="detail('."'".$list_array->id_sarana."'".')"><i class="fa fa-search"></i></button>';
			$data[] = $row;
		}

		$output = array(
			"draw" 				=> $_POST['draw'],
			"recordsTotal" 		=> $this->Model_fasyankes_list->count_all(),
			"recordsFiltered" 	=> $this->Model_fasyankes_list->count_filtered(),
			"data" 				=> $data,
		);
		echo json_encode($output);
	}

	public function detail($id_sarana){
		$is_permit = $this->aauth->control_no_redirect('fasyankes_list_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $perms      = "fasyankes_list_page";
        $comments   = "Data Detail Fasyankes";
        $this->aauth->logit($perms, current_url(), $comments);

        $this->data['bc_parent']    = "Fasyankes";
        $this->data['bc_child']     = "Detail";
    	$this->load->view('administration/view_fasyankes_detail', $this->data);
	}

}

/* End of file Fasyankes_list.php */
/* Location: ./application/controllers/administration/Fasyankes_list.php */
?>