<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_group extends CI_Controller {
	public function __construct() {
        parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('auth/login');
        }
        $this->load->model('Model_config_group');

        $this->data['users']            = $this->Menu_model->get_user($this->session->userdata('id'));
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['id_sarana']        = $this->data['users']->id_sarana;
    }

    public function index() {
        $is_permit = $this->aauth->control_no_redirect('Config_group_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $perms      = "Config_group_page";
        $comments   = "Group Management";
        $this->aauth->logit($perms, current_url(), $comments);

        $this->data['bc_parent']    = "Konfigurasi";
        $this->data['bc_child']     = "Group Management";
    	$this->load->view('administration/view_config_group', $this->data);
    }
    
    function ajax_list(){
        $list   = $this->Model_config_group->get_datatables();
        $data   = array();
        $no     = $_POST['start'];
        foreach($list as $list_array){
            $disable = $list_array->id < 3 ? "disabled" : "";
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $list_array->name;
            $row[]  = $list_array->definition;
            $edit   = '<button class="btn btn-primary btn-xs" title="Edit" onclick="edit('."'".$list_array->id."'".')"><i class="fa fa-edit"></i></button>';
            $delete = '<button class="btn btn-danger btn-xs" title="Edit" '.$disable.' onclick="remove('."'".$list_array->id."'".')"><i class="fa fa-trash"></i></button>';
            $row[]  = $edit."&nbsp;".$delete;
            $data[] = $row;
        }
        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->Model_config_group->count_all(),
            "recordsFiltered"   => $this->Model_config_group->count_filtered(),
            "data"              => $data,
        );
        echo json_encode($output);
    }
    
    function ajax_perm_create(){
        $is_permit = $this->aauth->control_no_redirect('Config_group_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $perm = $this->Model_config_group->get_list_perm();
        if(count($perm) > 0){
            foreach ($perm as $parent){
                $perm_child = $this->Model_config_group->get_list_perm($parent->id);
                $childs = array();
                if(count($perm_child) > 0){
                    foreach ($perm_child as $child){
                        $perm_child2 = $this->Model_config_group->get_list_perm($child->id);
                        $childs2 = array();
                        if(count($perm_child2) > 0){
                            foreach ($perm_child2 as $child2){
                                $childs2[] = array('id'=>$child2->id, 'text'=>$child2->definition, 'state'=>array('selected'=>false, 'opened'=>false));
                            }
                        }
                        $childs[] = array('id'=>$child->id, 'text'=>$child->definition, 'children'=>$childs2, 'state'=>array('selected'=>false, 'opened'=>false));
                    }
                }
                $data_valid[] = array('id'=>$parent->id, 'text'=>$parent->definition, 'children'=>$childs, 'state'=>array('selected'=>false, 'opened'=>false));
                
            }
            $success    = TRUE;
            $messages   = 'Ok';
        }else{
            $success    = FALSE;
            $data_valid = '';
            $messages   = '<h5 class="text-red">Tidak ada data permission</h5>';
        }
        $result = array(
            'success'       =>$success,
            'messages'      =>$messages,
            'data_valid'    =>$data_valid,
        );
        
        echo json_encode($result);
    }
    
    function do_create_group(){
        $is_permit = $this->aauth->control_no_redirect('Config_group_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $group_name     = $this->input->post('group_name',TRUE);
        $definition     = $this->input->post('definition',TRUE);
        $perms          = $this->input->post('tree_res',TRUE);
        $insertgroup    = $this->aauth->create_group($group_name, $definition);
        
        if($insertgroup){
            $arr_perm = explode(',', $perms);
            if(count($arr_perm) > 0){
                $hsl = false;
                foreach ($arr_perm as $perm_id){
                    $ins_permtogroup = $this->aauth->insert_perm_to_group($insertgroup, $perm_id);
                    if($ins_permtogroup){
                        $hsl = true;
                    }
                }
                
                if($hsl == true){
                    $res = array(
                        'success' => true,
                        'messages' => 'Group has been saved to database'
                    );

                    $perms = "Config_group_page";
                    $comments = "Success to Create a new group with group_id = '". $insertgroup ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                        'success' => false,
                        'messages' => 'Failed insert perm to group to database, please contact web administrator.'
                    );

                    $perms      = "Config_group_page";
                    $comments   = "Failed to Create perm to group when saving to database with post data = '". $insertgroup ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }else{
                $res = array(
                    'success' => true,
                    'messages' => 'Group has been saved to database'
                );

                $perms      = "Config_group_page";
                $comments   = "Success to Create a new group with group_id = '". $insertgroup ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }else{
            $res = array(
                'success'   => false,
                'messages'  => 'Failed insert user to database, please contact web administrator.'
            );

            $perms      = "user_management_view";
            $comments   = "Failed to Create a new user when saving to database with post data = '". json_encode($_REQUEST) ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
    
    function get_groupby_id(){
        $is_permit = $this->aauth->control_no_redirect('Config_group_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $group_id   = $this->input->post('group_id',TRUE);
        $datagroup  = $this->Model_config_group->get_group_by_id($group_id);
        if($datagroup){
            $res = array(
                'success'   => true,
                'data'      => $datagroup,
                'messages'  => 'Ok'
            );
        }else{
            $res = array(
                'success'   => false,
                'messages'  => 'ID Group Tidak Ditemukan'
            );
        }
        
        echo json_encode($res);
    }
    
    function ajax_perm_update(){
        $is_permit = $this->aauth->control_no_redirect('Config_group_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $group_id = $this->input->post('group_id',TRUE);
        $perm = $this->Model_config_group->get_list_perm();
        if(count($perm) > 0){
            $perm_group = $this->Model_config_group->get_perm_by_group($group_id);
            $perm_on = array();
            foreach ($perm_group as $perm_list){
                $perm_on[] = $perm_list->perm_id;
            }
            foreach ($perm as $parent){
                if (in_array($parent->id, $perm_on)){
                    $checked = true;
                }else{
                    $checked = false;
                }
                $perm_child = $this->Model_config_group->get_list_perm($parent->id);
                $childs = array();
                if(count($perm_child) > 0){
                    foreach ($perm_child as $child){
                        if (in_array($child->id, $perm_on)){
                            $checked1 = true;
                        }else{
                            $checked1 = false;
                        }
                        $perm_child2 = $this->Model_config_group->get_list_perm($child->id);
                        $childs2 = array();
                        if(count($perm_child2) > 0){
                            foreach ($perm_child2 as $child2){
                                if (in_array($child2->id, $perm_on)){
                                    $checked2 = true;
                                }else{
                                    $checked2 = false;
                                }
                                $childs2[] = array('id'=>$child2->id, 'text'=>$child2->definition, 'state'=>array('selected'=>$checked2, 'opened'=>false));
                            }
                        }
                        $childs[] = array('id'=>$child->id, 'text'=>$child->definition, 'children'=>$childs2, 'state'=>array('selected'=>$checked1, 'opened'=>false));
                    }
                }
                $data_valid[] = array('id'=>$parent->id, 'text'=>$parent->definition, 'children'=>$childs, 'state'=>array('selected'=>$checked, 'opened'=>false));
                
            }
            $success    = TRUE;
            $messages   = 'Ok';
        }else{
            $success        = FALSE;
            $data_valid     = '';
            $messages       = '<h5 class="text-red">Tidak ada data permission</h5>';
        }
        $result = array(
            'success'       =>$success,
            'messages'      =>$messages,
            'data_valid'    =>$data_valid,
        );
        echo json_encode($result);
    }
    
    function do_update_group(){
        $is_permit = $this->aauth->control_no_redirect('Config_group_page');
        if(!$is_permit) {
            redirect('no_permission');
            exit;
        }

        $group_id       = $this->input->post('group_id',TRUE);
        $group_name     = $this->input->post('group_name',TRUE);
        $definition     = $this->input->post('definition',TRUE);
        $perms          = $this->input->post('tree_res',TRUE);
        $updategroup    = $this->aauth->update_group($group_id, $group_name, $definition);
        
        if($updategroup){
            $this->Model_config_group->delete_perm_by_group($group_id);
            $arr_perm = explode(',', $perms);
            if(count($arr_perm) > 0){
                $hsl = false;
                foreach ($arr_perm as $perm_id){
                    $ins_permtogroup = $this->aauth->insert_perm_to_group($group_id, $perm_id);
                    if($ins_permtogroup){
                        $hsl = true;
                    }
                }
                if($hsl == true){
                    $res = array(
                        'success'   => true,
                        'messages'  => 'Group has been saved to database'
                    );

                    $perms      = "Config_group_page";
                    $comments   = "Success to update group with group_id = '". $group_id ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'success'   => false,
                    'messages'  => 'Failed insert perm to group to database, please contact web administrator.');

                    $perms      = "Config_group_page";
                    $comments   = "Failed to Create perm to group when saving to database with post data = '". $group_id ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }else{
                $res = array(
                    'success'   => true,
                    'messages'  => 'Group has been saved to database'
                );

                $perms      = "Config_group_page";
                $comments   = "Success to update group with group_id = '". $group_id ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }else{
            $res = array(
            'success'   => false,
            'messages'  => 'Failed update group to database, please contact web administrator.');

            $perms      = "user_management_view";
            $comments   = "Failed to update group when saving to database with post data = '". $group_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

}

/* End of file Config_group.php */
/* Location: ./application/controllers/config/Config_group.php */
?>