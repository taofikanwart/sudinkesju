<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct() {
        parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('auth/login');
        }
        $this->load->model('Model_profile');
        $this->load->model('Model_dashboard');  
        $this->load->model('Model_data_pegawai');

        $this->data['users']            = $this->aauth->get_user();
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    public function index() {
        $perms                      = "edit_Profilee_by_self";
        $comments                   = "Profile View";
        $this->aauth->logit($perms, current_url(), $comments);
        
        $this->data['bc_parent']    = "Profile";
        $this->data['bc_child']     = "";
        $this->load->view('view_profile', $this->data);
    }

    public function ajax_update_bio(){   
        $id_pegawai = $this->data['pegawai']->id_pegawai;

        $data = array(
            'nama_pegawai'  => $this->input->post('nama_lengkap',TRUE),
            'tempat_lahir'  => $this->input->post('tempat_lahir',TRUE),
            'tgl_lahir'     => date('Y-m-d',strtotime($this->input->post('tgl_lahir',TRUE))),
            'no_tlp'        => $this->input->post('no_tlp',TRUE),
            'alamat'        => $this->input->post('alamat',TRUE),
            'no_ktp'        => $this->input->post('no_ktp',TRUE),
            'npwp'          => $this->input->post('no_npwp',TRUE),
            'norek_dki'     => $this->input->post('no_rek',TRUE),
            'updated_by'    => $this->data['users']->id,
            'updated_date'  => date('Y-m-d H:i:s')
        );

        $data_user = array(
            'nama_lengkap'  => $this->input->post('nama_lengkap',TRUE),
        );
        $update = $this->Model_data_pegawai->update(array('id_pegawai' => $id_pegawai), $data);

        $nip  = $this->input->post('nip',TRUE);
        $this->Model_profile->update('aauth_users', $data_user, array('nip' => $nip));

        if ($update  == '1') {
            echo json_encode(array("status" => TRUE));    
        }else{
            echo json_encode(array("status" => FALSE));
        }
        
        $perms      = "edit_Profilee_by_self";
        $comments   = "Berhasil Edit Profil = '". json_encode($_REQUEST) ."'.";
        $this->aauth->logit($perms, current_url(), $comments);
    }

    public function ajax_update_ava(){
        $nip        = $this->input->post('nip',true);
        $path_old   = $this->input->post('path_old',true);
        echo $nip; die();
        $path       = '';
        $config['upload_path']      = 'data/foto_pegawai/';
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = 1024;
        $config['file_name']        = $nip;

        $this->load->library('upload', $config);
        if(!empty($_FILES['foto_file']['name'])){
            if (!$this->upload->do_upload('foto_file')){
                $status_upd_avatar  = $this->upload->display_errors();
            }else{
                if ($path_old != "") {
                    chmod($path_old, 0644);
                    unlink($path_old);    
                }
                $filedata   = $this->upload->data();
                $path       = 'data/foto_pegawai/'.$filedata['file_name'];
                //update ke db
                $data_avatar        = array(
                    'foto_url' => $path,
                );
                $update_avatar      = $this->Model_data_pegawai->update(array('id_pegawai' => $this->input->post('id_pegawai')), $data_avatar);
                $status_upd_avatar  = $update_avatar ? "Berhasil Update Avatar" : $this->upload->display_errors();
            }
        }else{
            $status_upd_avatar  = "Tidak ada File Avatar di Upload";
        }

        echo $path;
    }


    public function ajax_update_passwd(){
        echo "Upadate Password";
        
    }

    public function ajax_upload_dokumen($berkas){
        $nip            = $this->input->post('nip_pegawai');
        $path           = '';
        $path_old       = $this->Model_profile->get_profile($berkas, $this->data['pegawai']->id_pegawai);

        
        $config['upload_path']      = 'data/doc_pegawai/';
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = 1024;
        $config['file_name']        = $nip;
        $this->load->library('upload', $config);

        if(!empty($_FILES['berkas_upload']['name'])){
            if (!$this->upload->do_upload('berkas_upload')){
                $error = $this->upload->display_errors();
                $res = array(
                    'status'        => false,
                    'messages'      => $error
                    );
                echo json_encode($res);
                exit;
            }else{
                // unlink($path_old);
                $filedata   = $this->upload->data();
                $path       = 'data/doc_pegawai/'.$filedata['file_name'];
            }
        }



        $res = array(
            'success'       => true,
            'messages'      => 'Update berhasil'.$nip
        );
        echo json_encode($res);
    }

    public function ajax_get_username_byemail(){
        $email      = $this->input->post('email',TRUE);
        $old_data   = $this->Model_profile->get_by_email($email);
        
        if(count($old_data) > 0) {
            $res = array(
                'success'   => true,
                'messages'  => "Data found",
                'data'      => $old_data,
            );
        } else {
            $res = array(
                'success'   => false,
                'messages'  => "ID Tidak Ditemukan",
            );
        }
        echo json_encode($res);
    }
}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */
?>