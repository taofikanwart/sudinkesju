<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posting_artikel extends CI_Controller {
	public function __construct() {
        parent::__construct();
        
        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Silahkan Login Terlebih dahulu.');
            redirect('login');
        }
        $this->load->model('Model_posting_artikel');

        $this->data['users']            = $this->aauth->get_user();
        $this->data['groups']           = $this->aauth->get_user_groups();
        $this->data['list_menu_bar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['id_ukpd']          = $this->data['users']->id_ukpd;
        $this->data['pegawai']          = $this->Menu_model->get_pegawai_by_nip($this->data['users']->nip);
        $bagian                         = !empty($this->data['pegawai']) ? $this->data['pegawai']->bagian : "";
        $this->data['bagian']           = $this->Menu_model->get_bagian_by_id($bagian);
    }

    public function index() {
        $is_permit = $this->aauth->control_no_redirect('artikel_post_page');
        if(!$is_permit) {
            redirect('no_permission');
        }

        $perms                      = "artikel_post_page";
        $comments                   = "Posting Artikel";
        $this->aauth->logit($perms, current_url(), $comments);
        
        $this->data['bc_parent']    = "Artikel";
        $this->data['bc_child']     = "Posting Artikel";
        $this->data['kategori']		= $this->Model_posting_artikel->get_kategori();
        $this->load->view('view_posting_artikel', $this->data);
    }

    public function ajax_list(){
		$list 	= $this->Model_posting_artikel->get_datatables();
		$data 	= array();
		$no 	= $_POST['start'];
		foreach ($list as $list_array) {
			$no++;
			$row 	= array();
			$row[] 	= $no;
			$img 	= base_url().'assets/blog/imagespost/'.$list_array->image;
			$row[] 	= '<img src ="'.$img.'" width="80px">';
			$row[] 	= $list_array->title;
			$row[] 	= $list_array->kategori;
			$row[] 	= $list_array->added_by;
			$row[] 	= $list_array->add_date;
			$row[] 	= '<div class="btn-group">
              <button type="button" class="btn btn-info" onclick="view('."'".$list_array->idpost."'".')">Lihat</button>
              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu" role="menu">
                <li onclick="edit('."'".$list_array->idpost."'".')"><a href="#">Sunting</a></li>
                <li class="divider"></li>
                <li onclick="remove('."'".$list_array->idpost."'".')"><a href="#">Hapus</a></li>
              </ul>
             </div>';
			$data[] = $row;
		}

		$output = array(
			"draw" 				=> $_POST['draw'],
			"recordsTotal" 		=> $this->Model_posting_artikel->count_all(),
			"recordsFiltered" 	=> $this->Model_posting_artikel->count_filtered(),
			"data" 				=> $data,
		);
		echo json_encode($output);
	}

	public function ajax_add(){
		$config['upload_path']		="assets/blog/imagespost/";
        $config['allowed_types']	='gif|jpg|jpeg|png';
        $config['file_name'] 		= $this->input->post('judul');
        $this->load->library('upload',$config);
        
        if (!$this->upload->do_upload('image_file')){
        	//tidak ada file
	       $error 	= array('error' => $this->upload->display_errors());
	       echo json_encode(array("status" => $error));
	    }else{
	    	$filedata   = $this->upload->data();


		    $data['iduser']		= $this->data['users']->id;
			$data['title']		= $this->input->post('judul');
			$data['body']		= $this->input->post('artikel');
			$data['kategori']	= $this->input->post('kategori');
			$data['image']		= $filedata['file_name'];
			$data['added_by']	= $this->data['users']->nama_lengkap;

	        $insert = $this->Model_posting_artikel->save($data);
	        echo json_encode(array("status" => TRUE));
	    }
	}

	public function ajax_edit($idpost){
		$data = $this->Model_posting_artikel->get_by_id($idpost);
		echo json_encode($data);
	}

	public  function ajax_update(){
		$data['title']		= $this->input->post('judul');
		$data['body']		= $this->input->post('artikel');
		$data['kategori']	= $this->input->post('kategori');
		$data['edited_by']	= $this->data['users']->nama_lengkap;
		$data['edit_date']	= date("Y-m-d h:i:s");


		$config['upload_path']		="assets/blog/imagespost/";
        $config['allowed_types']	='gif|jpg|jpeg|png';
        $config['file_name'] 		= $this->input->post('judul');
        $this->load->library('upload',$config);
		if ($_FILES['image_file']['size'] != 0) {
			if (!$this->upload->do_upload('image_file')){
		       	$error 	= array('error' => $this->upload->display_errors());
	       		echo json_encode(array("status" => $error));
		    }else{
		    	$filelama			= $this->input->post('filelama');
		    	unlink('assets/blog/imagespost/'.$filelama);

		    	$filedata   		= $this->upload->data();
		    	$data['image']		= $filedata['file_name'];
		    }
		}

		$this->Model_posting_artikel->update(array('idpost' => $this->input->post('idpost')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($idpost){
		$this->Model_posting_artikel->delete_by_id($idpost);
		echo json_encode(array("status" => TRUE));
	}

}

/* End of file Posting_artikel.php */
/* Location: ./application/controllers/artikel/Posting_artikel.php */
?>