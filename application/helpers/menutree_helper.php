<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function build_tree(array $elements, $parentId = 0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = build_tree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }
    return $branch;
}

function print_tree($tree, $first=FALSE) {
    $CI = get_instance();
    $CI->load->model('Menu_model');

    if(!is_null($tree) && count($tree) > 0) {
        foreach($tree as $node) {
            if (current_url() == base_url().$node['url']) {
                $treeview_status        = "active";
                $treeview_menu_status   = "menu-open";
            }else{
                $treeview_status        = "";
                $treeview_menu_status   = "";
            }
            $a = $treeview_menu_status  == "menu-open" ? "active" : "";

            if(isset($node['children'])){
                //Menu Beranak
                //Cari Submenu yg aktive
                $sub_aktif = $CI->Menu_model->cari_sub_aktif($node['id']);
                $aktif     = $sub_aktif == "TRUE" ? "active" : "";


                echo '<li class="'.$aktif.' treeview">';
                echo    '<a href="#">';
                echo    '<i class="'.$node['icon'].'"></i>';
                echo        '<span>'.$node['definition'].'</span>';
                echo        '<span class="pull-right-container">';
                echo    '<i class="fa fa-angle-left pull-right"></i>';
                echo    '</span>';
                echo    '</a>';
                echo    '<ul class="treeview-menu">';
                print_tree($node['children']);
            }else{
                if($first){
                    //menu tidak beranak
                    echo '<li class="'.$treeview_status.'"><a href="'.base_url($node['url']).'"><i class="'.$node['icon'].'"></i> <span>'.$node['definition'].'</span></a></li>';
                }else{
                    
                    //submenu dari menu yg pertama
                    echo '<li class="'.$treeview_status.'"><a href="'.base_url($node['url']).'"><i class="'.$node['icon'].'"></i> '.$node['definition'].'</a></li>';
                }    
            }

            if(isset($node['children'])){
                echo '</ul>';
                echo '</li>';
            }else{
                echo '</li>';
            }
        }
    }
}

function print_tree_all($tree, $first=FALSE) {
    if(!is_null($tree) && count($tree) > 0) {
        foreach($tree as $node) {
            if(isset($node['children'])){
                //Menu Beranak
                echo "<li>
                        <span class=''>
                            <i class='fa fa-ellipsis-v'></i>
                            <i class='fa fa-ellipsis-v'></i>
                        </span>
                        <span class='text'>".$node['definition']."</span>
                        <div class='tools'>
                            <i class='fa fa-plus' onclick='tambah(".$node['id'].")'></i>
                            <i class='fa fa-edit' onclick='edit(".$node['id'].")'></i>
                            <i class='fa fa-trash-o' onclick='remove(".$node['id'].")'></i>
                        </div>
                    </li>";
                print_tree_all($node['children']);
            }else{
                if($first){
                    //menu tidak beranak
                    echo "<li>
                        <span class=''>
                            <i class='fa fa-ellipsis-v'></i>
                            <i class='fa fa-ellipsis-v'></i>
                        </span>
                        <span class='text'>".$node['definition']."</span>
                        <div class='tools'>
                            <i class='fa fa-plus' onclick='tambah(".$node['id'].")'></i>
                            <i class='fa fa-edit' onclick='edit(".$node['id'].")'></i>
                            <i class='fa fa-trash-o' onclick='remove(".$node['id'].")'></i>
                        </div>
                    </li>";
                }else{
                    //submenu dari menu yg pertama
                    echo "<li>
                        <span class=''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class='fa fa-ellipsis-v'></i>
                            <i class='fa fa-ellipsis-v'></i>
                        </span>
                        <span class='text'>&nbsp;&nbsp;".$node['definition']."</span>
                        <div class='tools'>
                            <i class='fa fa-edit' onclick='edit(".$node['id'].")'></i>
                            <i class='fa fa-trash-o' onclick='remove(".$node['id'].")'></i>
                        </div>
                    </li>";
                }    
            }
        }
    }


}