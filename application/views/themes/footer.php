<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('assets/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="<?php echo base_url('assets/AdminLTE/bootstrap/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/dist/js/app.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/pace/pace.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/morris/raphael-min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/morris/morris.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/daterangepicker/moment.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/select2/select2.full.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/input-mask/jquery.inputmask.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/fullcalendar/fullcalendar.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/data-tables/datatables.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/data-tables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/data-tables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/data-tables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/data-tables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/data-tables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/data-tables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/data-tables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/data-tables/datatables-init.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/input-mask/jquery.inputmask.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/input-mask/jquery.inputmask.date.extensions.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/input-mask/jquery.inputmask.extensions.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/fastclick/fastclick.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/clockpicker/bootstrap-clockpicker.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/jstree/dist/jstree.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/sweetalert/dist/sweetalert.min.js');?>"></script>

<script src="<?php echo base_url('assets/AdminLTE/plugins/iCheck/icheck.min.js');?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/dist/js/demo.js');?>"></script>

  <!-- </div>   -->
  <style type="text/css">
    .back-to-top {
      cursor: pointer;
      position: fixed;
      bottom: 20px;
      right: 20px;
      display:none;
  }
  </style>
  <a id="back-to-top" href="#" class="btn btn-info btn-sm back-to-top" role="button" title="Klik untuk kembali ke atas" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>

    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
      </div>
      <strong>Copyright &copy; 2019 <a href="#">Suku Dinas Kesehatan Jakarta Utara</a></strong> &nbsp;|&nbsp; Developed by <a href="https://wa.me/+6281223421479">Taofik Anwar</a> All rights reserved.
    </footer>


</body>
</html>

<script type="text/javascript">

  $(document).ajaxStart(function() { Pace.restart(); });
  
  $(document).ready(function(){
    $(function(e){
        var url=window.location;
        console.log(url);
        $('.treeview-menu a').each(function(e){
            var link = $(this).attr('href');
            if(link==url){
                $(this).parents('li').addClass('active');
                $(this).closest('.li').addClass('active');
            }
        });
    });

    $(window).scroll(function () {
      if ($(this).scrollTop() > 50) {
        $('#back-to-top').fadeIn();
      } else {
        $('#back-to-top').fadeOut();
      }
    });
        // scroll body to 0px on click
    $('#back-to-top').click(function () {
      $('#back-to-top').tooltip('hide');
      $('body,html').animate({
          scrollTop: 0
      }, 800);
      return false;
    });      
    $('#back-to-top').tooltip('show');
  });

	var ci_baseurl = '<?php echo base_url();?>';

  function numbersOnly(event){
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
  }

</script>