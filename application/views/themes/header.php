<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Suku Dinas Kesehatan Jakarta Utara</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link href="<?php echo base_url()?>favicon.ico" rel="icon">
  <link href="<?php echo base_url()?>favicon.ico" rel="apple-touch-icon">

  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bootstrap/css/bootstrap.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/font-awesome/css/font-awesome.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/Ionicons/css/ionicons.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/dist/css/AdminLTE.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/dist/css/skins/_all-skins.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/jstree/dist/themes/default/style.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/data-tables/dataTables.bootstrap.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/data-tables/buttons.bootstrap.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/data-tables/buttons.dataTables.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/select2/select2.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/morris/morris.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/fullcalendar/fullcalendar.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/datepicker/datepicker3.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/daterangepicker/daterangepicker.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/clockpicker/bootstrap-clockpicker.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/sweetalert/dist/sweetalert.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/jsautocomplete/js/jquery.autocomplete.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/pace/pace.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/iCheck/flat/blue.css');?>">  
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="#" class="logo">
      <span class="logo-mini"><b>SUDIN</b></span>
      <span class="logo-lg"><b>Suku Dinas</b>JU</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><b><?php echo $users->nama_lengkap." | " ?></b><small><?php echo $users->nama_sarana; ?></small></span>
            </a>
            <ul class="dropdown-menu">
              
              <li class="user-header">

                <p>
                  <?php echo $users->nama_lengkap; ?>
                  <small><?php echo $users->nama_sarana; ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('profile')?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('auth/login/do_logout')?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- <li class="dropdown user user-menu">
            <a href="https://wa.me/628119963335?text=Salam,%20Mohon%20bantuannya" target="_blank" class="dropdown-toggle"><i class="fa fa-whatsapp"></i><span>Whatsapp 0811 9963 335</span></a>
            </a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>
  
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <?php
          $list_menus = build_tree($list_menu_bar);
          print_tree($list_menus, TRUE);
        ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <div class="content-wrapper">
      <div class="alert alert-warning alert-dismissible" hidden id="peringatan" name="peringatan">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-warning"></i> Peringatan</h4>
        Batas Waktu Pengisian Aktifitas Aktif Setiap Tanggal :  <span id="tanggal" name="tanggal"></span>
      </div>
    <section class="content-header">
      <h1>
        <?php echo $bc_parent?><small><?php echo $bc_child?></small>
      </h1>
    </section>