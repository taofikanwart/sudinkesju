<?php $this->load->view('themes/header');?>

    <section class="content">
    	<div class="row">
    	<div class="col-md-6">
        	<div class="box box-default">        		
		        <div class="box-body">
		        	<div class="box-body">
			            <?php echo form_open('#',array('id' => 'form_profil','class'=> 'form-horizontal'))?>
					        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $users->id_sarana?>" readonly>

					        <div class="form-group">
					        	<label class="col-sm-4 control-label">Nama Fasyankes<span style="color: red"> *</span></label>
					        	<div class="col-sm-8">
					        	<select class="form-control" id="nama_fasyankes" name="nama_fasyankes">
						              <?php
						              $value = $this->Model_masterdata_pengguna->list_sarana();
						              foreach($value as $list){
						                  echo "<option value='".$list->id_sarana."'>".$list->nama_sarana."</option>";
						              }
						            ?>
						          </select>
					        	</div>
					        </div>

					        <div class="form-group">
					        	<label class="col-sm-4 control-label">Nama Direktur Utama/Kepala Puskesmas/ Penanggung Jawab Klink<span style="color: red"> *</span></label>
					        	<div class="col-sm-8">
					        	<input type="text" class="form-control" id="direktur_fasyankes" name="direktur_fasyankes" placeholder="Nama Direktur Utama/Kepala Puskesmas/ PenanggungJawab Klink">
					        	</div>
					        </div>

					        <div class="form-group" id="div_jenis" hidden="">
					          	<label class="col-sm-4 control-label">Jenis</label>
					          	<div class="col-sm-8">
					          	<select class="form-control" id="jenis_fasyankes" name="jenis_fasyankes">

					          	</select>
					          	</div>
					        </div>

					        <div class="form-group" id="div_kelasrs" hidden="">
					          	<label class="col-sm-4 control-label">Kelas RS</label>
					          	<div class="col-sm-8">
					          	<select class="form-control" id="kelasrs_fasyankes" name="kelasrs_fasyankes">
					          		<option value="A">Kelas A</option>
					          		<option value="B">Kelas B</option>
					          		<option value="C">Kelas C</option>
					          		<option value="D">Kelas D</option>
					          	</select>
					          	</div>
					        </div>

					        <div class="form-group" id="div_kepemilikan">
					          	<label class="col-sm-4 control-label">Kepemilikan</label>
					          	<div class="col-sm-8">
					          	<select class="form-control" id="kepemilikan_fasyankes" name="kepemilikan_fasyankes">
					          		<option value="bumn">BUMN</option>
					          		<option value="pempus">Pemerintah Pusat</option>
					          		<option value="pemda">Pemerintah Daerah</option>
					          		<option value="swasta">Swasta</option>
					          		<option value="tni">TNI / Polri</option>
					          	</select>
					          	</div>
					        </div>

					        <div class="form-group">
					          	<label class="col-sm-4 control-label">Nama Penyelenggara</label>
					          	<div class="col-sm-8">
					          	<input type="text" class="form-control" id="penyelenggara_fasyankes" name="penyelenggara_fasyankes" placeholder="Nama Penyelenggara">
					          	</div>
					        </div>

					        <div class="form-group">
					          	<label class="col-sm-4 control-label">Alamat</label>
					          	<div class="col-sm-8">
					          	<input type="text" class="form-control" id="alamat_fasyankes" name="alamat_fasyankes" placeholder="Alamat RS" rows='3'>
					          	</div>
					        </div>

					        <div class="form-group">
					          	<label class="col-sm-4 control-label">Kab/ Kota</label>
					          	<div class="col-sm-8">
					          	<input type="text" class="form-control" id="kota_fasyankes" name="kota_fasyankes" placeholder="Kab/ Kota" value="Jakarta Utara">
					          	</div>
					        </div>
					        
					        <div class="form-group">
					          	<label class="col-sm-4 control-label">Luas Tanah</label>
					          	<div class="col-sm-8">
					          		<div class="input-group">
					          			<input type="number" class="form-control" id="luas_tanah_fasyankes" name="luas_tanah_fasyankes" placeholder="Luas Tanah">
						                <span class="input-group-addon">m<sup>2</sup></span>
						            </div>
					          	
					          	</div>
					        </div>

					        <div class="form-group">
					          	<label class="col-sm-4 control-label">Luas Bangunan</label>
					          	<div class="col-sm-8">
					          		<div class="input-group">
					          			<input type="number" class="form-control" id="luas_bangunan_fasyankes" name="luas_bangunan_fasyankes" placeholder="Luas Bangunan">
						                <span class="input-group-addon">m<sup>2</sup></span>
						            </div>
					          	
					          	</div>
					        </div>
					        
					        <div class="form-group">
					          	<label class="col-sm-4 control-label">No. Telpon (call center)</label>
					          	<div class="col-sm-8">
					          	<input type="text" class="form-control" id="telp_fasyankes" name="telp_fasyankes" placeholder="No. Telpon (call center)">
					          	</div>
					        </div>
					        
					        <div class="form-group">
					          	<label class="col-sm-4 control-label">Alamat email</label>
					          	<div class="col-sm-8">
					          	<input type="email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="form-control" id="email_fasyankes" name="email_fasyankes" placeholder="Alamat email">
					          	</div>
					        </div>

					        <div class="form-group">
					          	<label class="col-sm-4 control-label">Alamat Website</label>
					          	<div class="col-sm-8">
					          	<input type="text" class="form-control" id="web_fasyankes" name="web_fasyankes" placeholder="Alamat Website">
					          	</div>
					        </div>
					        
					    <?php echo form_close()?>	
            		</div>
		        </div>
		        <div class="box-header">
                    <button name="btn_simpan" class="btn btn-primary pull-right" onclick="simpan_profil()"><i class="fa fa-save"></i>&nbsp; Simpan</button>
                </div>
	      	</div>
	    </div>

      	<div class="col-md-6">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#perizinan" data-toggle="tab">Perizinan dan Akreditasi</a></li>
              <li><a href="#tempattidur" data-toggle="tab">Tempat Tidur</a></li>
              <li><a href="#layanan" data-toggle="tab">Layanan</a></li>
              <li><a href="#lainnya" data-toggle="tab">Lainnya</a></li>

            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="perizinan">
                <?php echo form_open('#',array('id' => 'form_perizinan','class'=> 'form-horizontal'))?>
			        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $users->id_sarana?>" readonly>

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">No Surat Perizinan<span style="color: red"> *</span></label>
			        	<div class="col-sm-8">
			        	<input type="text" class="form-control" id="no_surat_izin" name="no_surat_izin" placeholder="No Surat Perizinan">
			        	</div>
			        </div>

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">Masa Berlaku Izin<span style="color: red"> *</span></label>
			        	<div class="col-sm-8">
			          		<div class="input-group">
			          			<input type="text" class="form-control datepicker" id="masa_berlaku_izin" name="masa_berlaku_izin" placeholder="Masa Berlaku Izin" readonly="">
				                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				            </div>
			          	</div>
			        </div>

			        <div class="form-group">
			          	<label class="col-sm-4 control-label">Status Akreditasi<span style="color: red"> *</span></label>
			          	<div class="col-sm-8">
			          	<select class="form-control" id="status_akre" name="status_akre">
			          		<option value="dasar">Dasar</option>
			          		<option value="madya">Madya</option>
			          		<option value="utama">Utama</option>
			          		<option value="paripurna">Paripurna</option>
			          	</select>
			          	</div>
			        </div>

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">No Sertifikat Akreditasi<span style="color: red"> *</span></label>
			        	<div class="col-sm-8">
			        	<input type="text" class="form-control" id="no_sertifikat_akre" name="no_sertifikat_akre" placeholder="No Sertifikat Akreditasi">
			        	</div>
			        </div>

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">Masa Berlaku Akreditasi<span style="color: red"> *</span></label>
			        	<div class="col-sm-8">
			          		<div class="input-group">
			          			<input type="text" class="form-control datepicker" id="masa_berlaku_akre" name="masa_berlaku_akre" placeholder="Masa Berlaku Akreditasi" readonly="">
				                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				            </div>
			          	</div>
			        </div>
			        
			    <?php echo form_close()?>
			    <div class="box-header">
                    <button name="btn_simpan" class="btn btn-primary pull-right" onclick="simpan_perizinan()"><i class="fa fa-save"></i>&nbsp; Simpan</button>
                </div>
          	</div>
          	<div class="tab-pane" id="tempattidur">
          		<div class="box-body">
	                <div class="row">
	                <div class="col-md-12">
	                    <div class="table-responsive m-t-40">
	                    <table id="table_bed" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
	                        <thead>
		                        <tr>
		                            <th style="width:15px;">No</th>
		                            <th>Kelas</th>
		                            <th>Jumlah</th>
		                        </tr>
	                        </thead>
	                        <tbody>
	                        	<tr>
	                            	<td colspan="3" style="text-align:center"> No Data to Display</td>
	                        	</tr>
	                        </tbody>
	                    </table>
	                    </div>
	                </div>
	                </div>
	            </div>
          	</div>
          	<div class="tab-pane" id="layanan">
	            <div class="box-header with-border">
	            	<div class="input-group">
	          			<input type="text" class="form-control" id="pelayanan" name="pelayanan" placeholder="Nama Unit Pelayanan">
		                <span onclick="tambah_layanan()" class="btn btn-primary input-group-addon"><i class="fa fa-save"></i></span>
		            </div>
	            </div>
	                <div class="box-body">
	                <div class="row">
	                <div class="col-md-12">
	                    <div class="table-responsive m-t-40">
	                    <table id="table" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
	                        <thead>
		                        <tr>
		                            <th style="width:15px;">No</th>
		                            <th>Pelayanan</th>
		                            <th >Hapus</th>
		                        </tr>
	                        </thead>
	                        <tbody>
		                        <tr>
		                            <td colspan="3" style="text-align:center"> No Data to Display</td>
		                        </tr>
	                        </tbody>
	                    </table>
	                    </div>
	                </div>
	                </div>
	            </div>
          	</div>

          	<div class="tab-pane" id="lainnya">
          	Lainnya
          	</div>
            </div>
          </div>
        </div>
        </div>
    </section>
  </div>

<?php $this->load->view('themes/footer');?>

<script type="text/javascript">
	$(document).ready(function(){
		get_data_profil();
		get_data_perizinan();

		$('.datepicker').datepicker({
	    	autoclose     : 'true',
	    	format        : "yyyy-mm-dd",
	    	startView     : "days", 
	      	minViewMode   : "days",
	    }); 

		$('#nama_fasyankes').on('mousedown', function(e) {
		   e.preventDefault();
		   this.blur();
		   window.focus();
		});

		table = $('#table').DataTable({ 
	        "processing"    : true,
	        "serverSide"    : true,
	        "scrollX"       : false,
	        "iDisplayLength": 25,
	        "paging"        : true,
	        "lengthChange"  : true,
	        "searching"     : true,
	        "ordering"      : true,
	        "info"          : true,
	        "autoWidth"     : true,
	        "order"         : [],
	        "lengthMenu"    : [[10, 20, 50, -1], [10, 20, 50, "All"]],
	        "ajax": {
	            "url"   : ci_baseurl + "administration/profil_sarana/ajax_list_layanan",
	            "type"  : "POST",
	            "data"  : function(d){
	            }
	        },
	    });

	    table_bed = $('#table_bed').DataTable({ 
	        "processing"    : true,
	        "serverSide"    : true,
	        "scrollX"       : false,
	        "paging"        : false,
	        "lengthChange"  : false,
	        "searching"     : false,
	        "ordering"      : false,
	        "info"          : false,
	        "autoWidth"     : true,
	        "ajax": {
	            "url"   : ci_baseurl + "administration/profil_sarana/ajax_list_bed",
	            "type"  : "POST",
	            "data"  : function(d){
	            }
	        },
	    });

	});

	function refresh(){
	    var table = $('#table').DataTable( );
	    table.ajax.reload(null,false);
	}

	function get_data_profil() {
		id = $('#id').val();

	    if(id !==''){ 
	      $.ajax({
	      url         : ci_baseurl + "administration/profil_sarana/ajax_get_profil",
	      type        : 'POST',
	      dataType    : 'JSON',
	      data        : {id:id},
	        success: function(data) {
	          var ret = data.success;
	          if(ret === true) {
	          	$('#nama_fasyankes').val(data.data.idsarana);
	            $('#direktur_fasyankes').val(data.data.direktur);

	            $('#jenis_fasyankes').find('option').remove();

	          	if (data.data.jenis_sarana == 'KLINIK') {
	          		$('#div_jenis').show();
	            	$('#jenis_fasyankes').val(data.data.jenis);

	            	$("#jenis_fasyankes").append('<option value="kpr">Klinik Pratama</option>');
	            	$("#jenis_fasyankes").append('<option value="kut">Klinik Utama</option>');
	          	}else if(data.data.jenis_sarana == 'RS'){
	          		$('#div_jenis').show();
	          		$('#div_kelasrs').show();

	            	$("#jenis_fasyankes").append('<option value="rsu">Rumah Sakit Umum</option>');
	            	$("#jenis_fasyankes").append('<option value="rsk">Rumah Sakit Khusus</option>');          		

	            	$('#jenis_fasyankes').val(data.data.jenis);
	            	$('#kelasrs_fasyankes').val(data.data.kelas);
	          	}else if(data.data.jenis_sarana == 'PUSKES'){
	          		$('#div_kepemilikan').hide();
	          	}
	            
	            $('#kepemilikan_fasyankes').val(data.data.kepemilikan);
	            $('#penyelenggara_fasyankes').val(data.data.penyelenggara);
	            $('#alamat_fasyankes').val(data.data.alamat);
	            $('#kota_fasyankes').val(data.data.kota);
	            $('#luas_tanah_fasyankes').val(data.data.luas_tanah);
	            $('#luas_bangunan_fasyankes').val(data.data.luas_bangunan);
	            $('#telp_fasyankes').val(data.data.telp);
	            $('#email_fasyankes').val(data.data.email);
	            $('#web_fasyankes').val(data.data.web);

	          } else {
	            swal('ID Data Tidak Ditemukan');
	          }
	        }
	      });
	    }else{
	      swal('ID Data Tidak Ditemukan');
	    }
	}

	function simpan_profil() {
	    swal({
	      title   : "Simpan Data",
	      text    : "Pastikan data yang terimput sudah benar",
	      type    : "warning",
	      showCancelButton  : true,
	      confirmButtonColor: "#DD6B55",
	      confirmButtonText : "Ya, Tambah",
	      closeOnConfirm    : false
	      },
	      function(){
	      $.ajax({
	        url     : ci_baseurl + "administration/profil_sarana/ajax_add",
	        type    : "POST",
	        dataType: "JSON",
	        data    : $('#form_profil').serialize(),
	        success: function(data){
	          var ret = data.status;
	          if(ret === true) {
	            swal({
	              title   : "Success!",
	              text    : "Data Berhasil Disimpan",
	              type    : "success",
	              timer   : 1500,
	            });
	            get_data();
	          } else {
	            swal("Data Gagal Ditambahkan");
	          }
	        },
	        error: function (jqXHR, textStatus, errorThrown, data){
	          swal("Gagal", "Data Gagal Disimpan", "error");
	        }
	      });    
	    });
	}

	function get_data_perizinan() {
		id = $('#id').val();

	    if(id !==''){ 
	      $.ajax({
	      url         : ci_baseurl + "administration/profil_sarana/ajax_get_perizinan",
	      type        : 'POST',
	      dataType    : 'JSON',
	      data        : {id:id},
	        success: function(data) {
	          var ret = data.success;
	          if(ret === true) {
	          	$('#no_surat_izin').val(data.data.no_surat_izin);
	            $('#masa_berlaku_izin').val(data.data.masa_berlaku_izin);
	            $('#status_akre').val(data.data.status_akre);
	            $('#no_sertifikat_akre').val(data.data.no_sertifikat_akre);
	            $('#masa_berlaku_akre').val(data.data.masa_berlaku_akre);
	          } else {
	            swal('ID Data Tidak Ditemukan');
	          }
	        }
	      });
	    }else{
	      swal('ID Data Tidak Ditemukan');
	    }
	}

	function simpan_perizinan() {
		id 					= $('#id').val();
		no_surat_izin 		= $('#no_surat_izin').val();
		masa_berlaku_izin 	= $('#masa_berlaku_izin').val();
		status_akre			= $('#status_akre').val();
		no_sertifikat_akre 	= $('#no_sertifikat_akre').val();
		masa_berlaku_akre 	= $('#masa_berlaku_akre').val();

		if (no_surat_izin!="" && masa_berlaku_izin!="" && status_akre!="" && no_sertifikat_akre!="" && masa_berlaku_akre!="") {
			swal({
		      title   : "Simpan Data",
		      text    : "Pastikan data yang terimput sudah benar",
		      type    : "warning",
		      showCancelButton  : true,
		      confirmButtonColor: "#DD6B55",
		      confirmButtonText : "Ya, Tambah",
		      closeOnConfirm    : false
		      },
		      function(){
		      $.ajax({
		        url     : ci_baseurl + "administration/profil_sarana/ajax_add_perizinan",
		        type    : "POST",
		        dataType: "JSON",
		        data    : $('#form_perizinan').serialize()+ "&id=" +id,
		        success: function(data){
		          var ret = data.status;
		          if(ret === true) {
		            swal({
		              title   : "Success!",
		              text    : "Data Berhasil Disimpan",
		              type    : "success",
		              timer   : 1500,
		            });
		            get_data_perizinan();
		          } else {
		            swal("Data Gagal Ditambahkan");
		          }
		        },
		        error: function (jqXHR, textStatus, errorThrown, data){
		          swal("Gagal", "Data Gagal Disimpan", "error");
		        }
		      });    
		    });		
		}else{
			swal('Pemberitahuan!','Pastikan data yg diinput sudah lengkap','warning');
		}
	}

	function tambah_layanan() {
		id 				= $('#id').val();
		layanan 		= $('#pelayanan').val();

		$.ajax({
	        url     : ci_baseurl + "administration/profil_sarana/ajax_add_layanan",
	        type    : "POST",
	        dataType: "JSON",
	        data    : {id:id, layanan:layanan},
	        success: function(data){
	          var ret = data.status;
	          if(ret === true) {
	            swal({
	              title   : "Success!",
	              text    : "Data Berhasil Disimpan",
	              type    : "success",
	              timer   : 1500,
	            });
	            refresh();
	          } else {
	            swal("Data Gagal Ditambahkan");
	          }
	        },
	        error: function (jqXHR, textStatus, errorThrown, data){
	          swal("Gagal", "Data Gagal Disimpan", "error");
	        }
	    });    
	}	
	
	function remove_layanan(id) {
	    swal({
	      title   : "Yakin Akan Di Hapus?",
	      text    : "Data yg sudah di hapus tidak bisa di kembalikan",
	      type    : "warning",
	      showCancelButton  : true,
	      confirmButtonColor: "#DD6B55",
	      confirmButtonText : "Ya, Hapus",
	      closeOnConfirm    : false
	    },
	    function(){
	      	$.ajax({
		        url : ci_baseurl + "administration/profil_sarana/ajax_delete_layanan/"+id,
		        type: "POST",
		        dataType: "JSON",
		        success: function(data){
		          refresh();
		          swal({
		            title   : "Success!",
		            text    : "Data Berhasil Dihapus",
		            type    : "success",
		            timer   : 1500,
		          });
		        },
		        error: function (jqXHR, textStatus, errorThrown){
		          swal("Gagal", "Data Gagal Dihapus", "error");
		          refresh();
		        }
	      	});
		});
  	}

  	function autosave(id_kelas) {
  		value = $("#"+id_kelas).val();

  		$.ajax({
	        url     : ci_baseurl + "administration/profil_sarana/ajax_autosave",
	        type    : "POST",
	        dataType: "JSON",
	        data    : {id_kelas:id_kelas, value:value},
	          success: function(data){
	            var ret = data.status;
	            if(ret === true) {
	              console.log("data tersimpan");
	            } else {
	              swal("Data Gagal Ditambahkan");
	            }
	          },
	          error: function (jqXHR, textStatus, errorThrown, data){
	            swal("Gagal", "Data Gagal Disimpan", "error");
	          }
        });    
  	}
</script>