<?php $this->load->view('themes/header');?>

    <section class="content">
		<div class="row">
	        <div class="col-md-12">
	        	<div class="box box-default">
	        		<div class="box-header">
	        			<button type="button" id="btn_tambah" name="btn_tambah" class="btn btn-primary" onclick="tambah()"><i class="fa fa-plus"></i> Tambah</button>
	        			<button type="button" id="btn_tambah" name="btn_tambah" class="btn btn-info" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>
	        		</div>
		            <div class="box-body">
			        	<div class="row">
			        		<div class="col-md-12">
			        			<div class="table-responsive m-t-40">
						            <table id="table" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
						                <thead>
				                            <tr>
				                                <th>No</th>
				                                <th>Group Name</th>
				                                <th>Definition</th>
				                                <th>Action</th>
				                            </tr>
				                        </thead>
				                        <tbody>
				                            <tr>
				                                <td colspan="4"> No Data to Display</td>
				                            </tr>
				                        </tbody>
						             </table>
						        </div>
			        		</div>
			        	</div>
			        </div>
	            </div>  
	      	</div>
	    </div>
  	</section>
</div>

<?php $this->load->view('themes/footer');?>

<!-- MODAL -->
<div class="modal fade" id="defaultModal" role="dialog">
  	<div class="modal-dialog modal-m" role="document">
      	<div class="modal-content">
        <div class="modal-header">
            <button class="pull-right btn bg-red" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title" id="defaultModalLabel">Tambah</h4>
        </div>
          
        <div class="modal-body form">
            <?php echo form_open('#',array('id' => 'formGroup','class'=> 'form-horizontal', 'enctype' => 'multipart/form-data'))?>
                <div class="form-body">
	                <div class="col-lg-12">
	                    <span style="color: red">* Required Field</span>
	                </div>
	                <div class="col-lg-12">
	                    <div class="form-group form-material">
	                    	<input type="hidden" name="group_id" id="group_id" readonly>
	                        <label class="control-label">Group Name <span style="color: red">*</span></label>
	                        <input type="text" class="form-control" name="group_name" id="group_name" placeholder="Group Name is required">
	                    </div>
	                </div>
	                <div class="col-lg-12">
	                    <div class="form-group form-material">
	                        <label class="control-label">Definition <span style="color: red">*</span></label>
	                        <input type="text" class="form-control" name="definition" id="definition" placeholder="Definition is required">
	                    </div>
	                </div>
	                <div class="col-lg-12">
	                    <h4 class="example-title">Permission</h4>
	                    <fieldset>
	                        <div id="perm_list">
	                            
	                        </div>
	                    </fieldset>
	                </div>
                </div>
            <?php echo form_close()?>
        </div>
          	<div class="modal-footer">
          	<button type="button" id="btn_simpan" class="btn bg-blue waves-effect" onclick="proses()" title="Isi Semua Untuk Menyimpan">Simpan</button>
          	<button type="button" id="btn_batal" class="btn bg-red waves-effect" data-dismiss="modal">Batal</button>
          	</div>
      	</div>
  	</div>
</div>


<!-- SCRIPT -->
<script type="text/javascript">
	var prosesmethod; 
  	var table;

	$(document).ready(function(){
		table = $('#table').DataTable({ 
		    "processing"    : true,
		    "serverSide"    : true,
		    "scrollX" 		: true,
      		"iDisplayLength": 25,
		    "paging"        : true,
		    "lengthChange"  : true,
		    "searching"     : true,
		    "ordering"      : true,
		    "info"          : true,
		    "autoWidth"     : true,
		    "order"         : [],
		    "lengthMenu"    : [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
		    "columnDefs" 	: [{ "searchable": true, "targets": 0, }],
		    "ajax": {
		        "url" : ci_baseurl + "administration/config_group/ajax_list",
		        "type": "POST",
		        "data"  : function(d){
		            // d.interval = $("#interval").val();
		        }
		    },
	        dom: 'Bfrtip',buttons: [
	          {extend: 'print',exportOptions: {columns: ':visible'}},
	          {extend: 'excel',exportOptions: {columns: ':visible'}},
	          {extend: 'pdf'  ,exportOptions: {columns: [ 0,1,2,3,4,5 ]}}, 
	          'pageLength'],
	    });

	    var table = $('#table').DataTable();
	    table.columns().every( function () {
	        var that = this;
	        $( 'cari', this.footer() ).on( 'keyup change', function () {
	            if (that.search() !== this.value) {
	                that.search(this.value ).draw();
	            }
	        } );
	    } );
	});

	function refresh(){
	    var table = $('#table').DataTable( );
	    table.ajax.reload(null,false);
	}

	function tambah() {
		$('#formGroup').trigger("reset");
		perm_create();
		$('.modal-title').text('Tambah Data');
		$('#defaultModal').modal('show');
		prosesmethod = "tambah";
	}

	function remove(id_group) {
		swal({
	      	title   : "Tidak Dapat Dihapus",
	      	text    : "Fitur in belum di aktifkan",
	      	type    : "warning",
	      	showCancelButton  : false,
	      	confirmButtonColor: "#DD6B55",
	      	confirmButtonText : "Ok, Baiklah",
	      	closeOnConfirm    : false
	    });
	}

	function proses() {
		if (prosesmethod == "tambah") {
			swal({
            title               : "Simpan Data",
            text                : "Yakin Simpan Data Berikut ?",
            type                : "warning",
            showCancelButton    : true,
            confirmButtonColor  : "#DD6B55",
            confirmButtonText   : "Ya, Simpan",
            cancelButtonText    : 'Batal',
            closeOnConfirm      : false
        },
        function(){
            var result = $('#perm_list').jstree('get_selected');
            $.ajax({
                url: ci_baseurl + "administration/config_group/do_create_group",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#formGroup').serialize()+ "&tree_res=" +result,
                success: function (data) {
                    var ret = data.success;
                    if (ret === true) {
                        swal({
                            title   : "Success!",
                            text    : "Data Berhasil Disimpan",
                            type    : "success",
                            timer   : 1500,
                        });
                        $('#formGroup').find("select").val("");
                        $('#formGroup').find("input[type=text]").val("");
                        $('#formGroup').find("textarea").val("");
                        refresh();
                    } else {
                        swal(data.messages.replace(/<p>|<\/p>/gi, ""));
                    }
                }
            });
        });
		}else if(prosesmethod == "edit"){
			swal({
	            title               : "Update Data",
	            text                : "Yakin Update Data Berikut ?",
	            type                : "warning",
	            showCancelButton    : true,
	            confirmButtonColor  : "#DD6B55",
	            confirmButtonText   : "Ya, Update",
	            cancelButtonText    : 'Batal',
	            closeOnConfirm      : false
	        },
	        function(){
	            var result = $('#perm_list').jstree('get_selected');
	                $.ajax({
	                url 	: ci_baseurl + "administration/config_group/do_update_group",
	                type 	: 'POST',
	                dataType: 'JSON',
	                data 	: $('form#formGroup').serialize()+ "&tree_res=" +result,
	                success: function (data) {
	                    var ret = data.success;
	                    if (ret === true) {
	                        swal({
	                            title   : "Success!",
	                            text    : "Data Berhasil Disimpan",
	                            type    : "success",
	                            timer   : 1500,
	                        });
	                        refresh();
	                    } else {
	                        swal(data.messages.replace(/<p>|<\/p>/gi, ""));
	                    }
	                }
	            });
	        });
		}else{
			swal("Perintah Tidak Diketahui");
		}
	}

	function perm_create(){
	    $.ajax({
	        url 	: ci_baseurl + "administration/config_group/ajax_perm_create",
	        type 	: 'get',
	        dataType: 'json',
	        data 	: {}, 
	        success: function(data) {
	            $('#perm_list').html("");
	            $.jstree.destroy();
	            var ret = data.success;
	            if(ret === true){
	                jstree = $("#perm_list").jstree({
	                    "core" 		: {"data" : data.data_valid },
	                    "checkbox" 	: {"keep_selected_style" : false, "three_state" : false,},
	                    "plugins" 	: [ "checkbox" ]
	                });
	            }else{
	                $('#perm_list').html(data.messages);
	            }
	        }
	    });
	}

	function edit(group_id){
	    if(group_id){    
	        $.ajax({
	        url 	: ci_baseurl + "administration/config_group/get_groupby_id",
	        type 	: 'POST',
	        dataType: 'JSON',
	        data 	: {group_id:group_id},
	            success: function(data) {
	                var ret = data.success;
	                if(ret === true) {
	                    $('#group_id').val(data.data.id);
	                    $('#group_name').val(data.data.name);
	                    $('#definition').val(data.data.definition);

	                    $('.modal-title').text('Sunting Data');
					    $('#defaultModal').modal('show');
					    prosesmethod = "edit";

	                    perm_update(group_id);
	                } else {
	                    swal(data.messages.replace(/<p>|<\/p>/gi, ""));
	                }
	            }
	        });
	    }else{
	        swal("ID Group Tidak Ditemukan");
	    }
	}

	function perm_update(group_id){
	    $.ajax({
	        url 	: ci_baseurl + "administration/config_group/ajax_perm_update",
	        type 	: 'POST',
	        dataType: 'JSON',
	        data 	: {group_id:group_id}, 
	        success: function(data) {
	            $('#perm_list').html("");
	            $.jstree.destroy();
	            var ret = data.success;
	            if(ret === true){
	                jstree = $("#perm_list").jstree({
	                    "core" 		: {"data" : data.data_valid},
	                    "checkbox" 	: {"keep_selected_style" : false,"three_state" : false,},
	                    "plugins" 	: [ "checkbox" ]
	                });
	            }else{
	                $('#perm_list').html(data.messages);
	            }
	        }
	    });
	}
</script>