<?php $this->load->view('themes/header');?>

    <section class="content">
        <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            <div class="box-header with-border">
              <!-- <button type="button" id="btn_tambah" name="btn_tambah" class="btn btn-primary" onclick="tambah()"><i class="fa fa-plus"></i> Tambah</button> -->
              <button type="button" id="btn_refresh" name="btn_refresh" class="btn btn-info" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>
            </div>
                <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive m-t-40">
                    <table id="table" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Jenis</th>
                            <th>Kelas</th>
                            <th>Kepemilikan</th>
                            <th>Telp</th>
                            <th>Email</th>
                            <th style="width:15px;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="8" style="text-align:center"> No Data to Display</td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                </div>
            </div>
            </div>  
        </div>
        </div>
    </section>
  </div>

<?php $this->load->view('themes/footer');?>

<script type="text/javascript">
  var table;

  $(document).ready(function(){

    table = $('#table').DataTable({ 
        "processing"    : true,
        "serverSide"    : true,
        "scrollX"       : true,
        "iDisplayLength": 25,
        "paging"        : true,
        "lengthChange"  : true,
        "searching"     : true,
        "ordering"      : true,
        "info"          : true,
        "autoWidth"     : true,
        "order"         : [],
        "lengthMenu"    : [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        "columnDefs"  : [{ "searchable": true, "targets": 0, }],
        "ajax": {
            "url"   : ci_baseurl + "administration/fasyankes_list/ajax_list",
            "type"  : "POST",
            "data"  : function(d){
            }
        },
        dom: 'Bfrtip',buttons: [
          {extend: 'print',exportOptions: {columns: [ 1,2,3,4 ]}}, 
          {extend: 'excel',exportOptions: {columns: [ 1,2,3,4 ]}}, 
          {extend: 'pdf'  ,exportOptions: {columns: [ 1,2,3,4 ]}}, 
          'pageLength'],
      });

      var table = $('#table').DataTable();
      table.columns().every( function () {
          var that = this;
          $( 'cari', this.footer() ).on( 'keyup change', function () {
              if (that.search() !== this.value) {
                  that.search(this.value ).draw();
              }
          } );
      } );
  });

  function detail(id) {
      window.open(ci_baseurl + "administration/fasyankes_list/detail/"+id);
  }

</script>