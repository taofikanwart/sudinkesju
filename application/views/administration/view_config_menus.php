<?php $this->load->view('themes/header');?>

    <section class="content">
      <div class="row">
        <!-- DARI SINI -->
        <div class="col-md-6">
        	<div class="box box-default">
        		<div class="box-header">
                    <button name="btn_tambah" class="btn btn-primary" onclick="tambah(0)"><i class="glyphicon glyphicon-plus"></i>&nbsp; Tambah</button>
                </div>
		        <div class="box-body">
		        	<div class="box-body">
			            <ul class="todo-list ui-sortable">
			                <?php
					        	$menu_all = build_tree($list_menu_all);
			          			print_tree_all($menu_all, TRUE);
					        ?>
			            </ul>
            		</div>
		        </div>
	      	</div>
	      </div>
        <!-- SAMPAI SINI -->
      </div>
    </section>
</div>

<!-- MODAL -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-m" role="document">
      <div class="modal-content">
          <div class="modal-header">
            <button class="pull-right btn bg-red" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title" id="defaultModalLabel">Tambah</h4>
          </div>
          <div class="modal-body form">
           	<form action="#" id="formmodal" name="formmodal" class="form-horizontal" method="">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="id">ID</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="id" name="id" placeholder="Auto" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="name">Auth</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Auth" required onkeyup="cekkosong()">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="definition">Nama Menu</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="definition" name="definition" placeholder="Nama Menu" required onkeyup="cekkosong()">
                    </div>
                </div>      

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="parent_id">Parent Menu</label>
                    <div class="col-sm-8">
                    <select class="form-control" id="parent_id" name="parent_id">
                    	<option value="0">:: Parent Menu ::</option>	
                    	<?php foreach ($menu_parent as $list) { ?>
                    		<option value="<?php echo $list['id']?>"><?php echo $list['definition']?></option>	
                    	<?php }?>
	                    
	                  </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="url">URL Contorller</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="url" name="url" placeholder="URL contoller" required onkeyup="cekkosong()">
                    </div>
                </div>      

                  

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="icon">Icon</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="icon" name="icon" placeholder="Icon" readonly required onkeyup="cekkosong()">
                    </div>
                </div>          
            </form>
        </div>
	      	<div class="form-group">
	            <label class="col-sm-4 control-label" >&nbsp;</label>
	            <div class="col-sm-8">
	            	<div class="text-center">
	        		<div id="divkumpulanicon" name="divkumpulanicon">
			        <table class="table pull-left">
			        <tbody>
			        <tr>
			          <td><button class="btn btn-default" id="fa fa-dashboard" onclick="set_icon('fa fa-dashboard')"><i class="fa fa-dashboard"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-desktop" onclick="set_icon('fa fa-desktop')"><i class="fa fa-desktop"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-money" onclick="set_icon('fa fa-money')"><i class="fa fa-money"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-cogs" onclick="set_icon('fa fa-cogs')"><i class="fa fa-cogs"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-inbox" onclick="set_icon('fa fa-inbox')"><i class="fa fa-inbox"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-folder" onclick="set_icon('fa fa-folder')"><i class="fa fa-folder"></i></button></td>
			        </tr>
			        <tr>
			          <td><button class="btn btn-default" id="fa fa-file-pdf-o" onclick="set_icon('fa fa-file-pdf-o')"><i class="fa fa-file-pdf-o"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-group" onclick="set_icon('fa fa-group')"><i class="fa fa-group"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-balance-scale" onclick="set_icon('fa fa-balance-scale')"><i class="fa fa-balance-scale"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-recycle" onclick="set_icon('fa fa-recycle')"><i class="fa fa-recycle"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-pie-chart" onclick="set_icon('fa fa-pie-chart')"><i class="fa fa-pie-chart"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-bar-chart" onclick="set_icon('fa fa-bar-chart')"><i class="fa fa-bar-chart"></i></button></td>    
			        </tr>
			        <tr>
			          <td><button class="btn btn-default" id="fa fa-area-chart" onclick="set_icon('fa fa-area-chart')"><i class="fa fa-area-chart"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-files-o" onclick="set_icon('fa fa-files-o')"><i class="fa fa-files-o"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-th" onclick="set_icon('fa fa-th')"><i class="fa fa-th"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-table" onclick="set_icon('fa fa-table')"><i class="fa fa-table"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-calendar" onclick="set_icon('fa fa-calendar')"><i class="fa fa-calendar"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-book" onclick="set_icon('fa fa-book')"><i class="fa fa-book"></i></button></td>
			        </tr>
			        <tr>
			          <td><button class="btn btn-default" id="fa fa-database" onclick="set_icon('fa fa-database')"><i class="fa fa-database"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-user-plus" onclick="set_icon('fa fa-user-plus')"><i class="fa fa-user-plus"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-file-text-o" onclick="set_icon('fa fa-file-text-o')"><i class="fa fa-file-text-o"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-arrows-alt" onclick="set_icon('fa fa-arrows-alt')"><i class="fa fa-arrows-alt"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-sticky-note-o" onclick="set_icon('fa fa-sticky-note-o')"><i class="fa fa-sticky-note-o"></i></button></td>
			          <td><button class="btn btn-default" id="fa fa-archive" onclick="set_icon('fa fa-archive')"><i class="fa fa-archive"></i></button></td>
			        </tr>
			        </tbody>
			        </table>
			        </div>
		            </div>
	            </div>
	        </div>    
          
        	<div class="modal-footer">
	            <button type="button" id="btn_simpan" class="btn bg-blue waves-effect" onclick="proses()" title="Isi Semua Untuk Menyimpan">Simpan</button>
	            <button type="reset" id="btn_reset" class="btn btn-default waves-effect" onclick="modal_reset()">Reset</button>
	            <button type="button" id="btn_batal" class="btn bg-red waves-effect" data-dismiss="modal">Batal</button>
        	</div>
      	</div>
  	</div>
</div>
<!-- MODAL -->

<?php $this->load->view('themes/footer');?>

<!-- SCRIPT -->
<script type="text/javascript">
	function cekkosong() {
        name        = $('#name').val();
        definition  = $('#definition').val();
        url        	= $('#url').val();
        
        if (name == "" || definition == "" || url == "") {
            $('#btn_simpan').prop("disabled", true);    
        }else{
            $('#btn_simpan').prop("disabled", false);
        }
    }

    function set_icon(faicon) {
    	$('#icon').val(faicon);
    	cekkosong();
    }

    function modal_reset(){
        $('#btn_simpan').prop("disabled", true);
        $('#formmodal').trigger("reset");
    }

    function modal_load(){
        $('#btn_simpan').show();
        $('#btn_reset').show();
        $('#formmodal :input').attr("disabled", false);
        $('#defaultModal').modal('show');
    }

    function tambah(id) {
        modal_reset();
        modal_load();
        proses_method = "tambah";
        $('.modal-title').text('Tambah Data');
        $('#parent_id').val(id);
    }

	function edit(id) {
		$('#btn_reset').hide();
        if(id !==''){ 
            $.ajax({
            url         : ci_baseurl + "administration/config_menu/ajax_get_by_id",
            type        : 'POST',
            dataType    : 'JSON',
            data        : {id:id},
                success: function(data) {
                    var ret = data.success;
                    if(ret === true) {
                      $('#id').val(data.data.id);
                      $('#name').val(data.data.name);
                      $('#definition').val(data.data.definition);
                      $('#parent_id').val(data.data.parent_id);
                      $('#url').val(data.data.url);
                      $('#icon').val(data.data.icon);
                      
                      $('.modal-title').text('Edit Data');
                      modal_load();
                      proses_method = "edit";
                    } else {
                      swal("Data Tidak Ditemukan");
                    }
                }
            });
        }else{
          swal("ID Pasien Tidak Ditemukan");
        }   
	}

	function remove(id) {
		swal({
            title   : "Yakin Akan Di Hapus?",
            text    : "Data yg sudah di hapus tidak bisa di kembalikan",
            type    : "warning",
            showCancelButton  : true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText : "Ya, Hapus",
            closeOnConfirm    : false
        },
        function(){
            $.ajax({
            url     : ci_baseurl + "administration/config_menu/ajax_delete/"+id,
            type    : "GET",
            dataType: "JSON",
            success : function(data){
                swal({
                    title   : "Success!",
                    text    : "Data Berhasil Dihapus",
                    type    : "success",
                    timer   : 1000,
                });
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal("Gagal", "Data Gagal Dihapus", "error");
            }
            });
        });
	}

	function proses() {
		if (proses_method == "tambah") {
			swal({
                title   : "Tambah Data ?",
                text    : "Data Ditambahkan, Pastikan Data Terinput Dengan Benar",
                type    : "warning",
                showCancelButton  : true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText : "Ya, Tambah",
                closeOnConfirm    : false
            },
                function(){
                $.ajax({
                url     : ci_baseurl + "administration/config_menu/ajax_add",
                type    : "POST",
                dataType: "JSON",
                data    : $('#formmodal').serialize(),
                success : function(data){
                    var ret = data.status;
                    if(ret === true) {
                        swal({
                            title   : "Success!",
                            text    : "Data Berhasil Disimpan",
                            type    : "success",
                            timer   : 1000,
                        });
                        $('#defaultModal').modal('hide');
                    }else{
                        swal("Data Gagal Ditambahkan");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown, data){
                    swal("Gagal", "Data Gagal Disimpan", "error");
                }
                });
            });
		}else if (proses_method=="edit") {
			swal({
                title   : "Sunting Data ?",
                text    : "Data Disunting, Pastikan Data Terinput Dengan Benar",
                type    : "warning",
                showCancelButton  : true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText : "Ya, Sunting",
                closeOnConfirm    : false
            },
                function(){
                $.ajax({
                url     : ci_baseurl + "administration/config_menu/ajax_update",
                type    : "POST",
                dataType: "JSON",
                data    : $('#formmodal').serialize(),
                success : function(data){
                    var ret = data.status;
                    if(ret === true) {
                        swal({
                            title   : "Success!",
                            text    : "Data Berhasil Disimpan",
                            type    : "success",
                            timer   : 1000,
                        });
                        $('#defaultModal').modal('hide');
                    }else{
                        swal("Data Gagal Di Update");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown, data){
                    swal("Gagal", "Data Gagal Disimpan", "error");
                }
                });
            });
		}else{
			swal("Proses Method Tidak Diketahui");
		}
	}
</script>