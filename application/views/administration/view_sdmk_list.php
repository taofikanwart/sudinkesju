<?php $this->load->view('themes/header');?>

    <section class="content">
        <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            <div class="box-header with-border">
              <button type="button" id="btn_import" name="btn_import" class="btn btn-primary" onclick="modalload()"><i class="fa fa-file-excel-o"></i> Import</button>
              <!-- <a href="<?php echo base_url();?>administration/sdmk_list/download_format_import_sdmk" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Download Format</a> -->
              <button type="button" id="btn_refresh" name="btn_refresh" class="btn btn-info" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>
            </div>
                <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive m-t-40">
                    <table id="table" class="display compact table table-hover table-striped table-bordered text-center" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                             <th class="align-middle" rowspan="4">No.</th>
                             <th class="align-middle" rowspan="4">Kabupaten/Kota</th>
                             <th colspan="52">Tenaga Kesehatan</th>
                             <th colspan="28">Asisten Tenaga Kesehatan</th>
                             <th colspan="12">Tenaga Penunjang</th>
                             <th colspan="4" rowspan="2">Total Per Kabupaten</th>
                             </tr>
                           <tr>
                             <th colspan="4">Medis</th>
                             <th colspan="4">Psikologi Klinis</th>
                             <th colspan="4">Keperawatan</th>
                             <th colspan="4">Kebidanan</th>
                             <th colspan="4">Kefarmasian</th>
                             <th colspan="4">Kesehatan Masyarakat</th>
                             <th colspan="4">Kesehatan Lingkungan</th>
                             <th colspan="4">Gizi</th>
                             <th colspan="4">Keterapian Fisik</th>
                             <th colspan="4">Keteknisian Medis</th>
                             <th colspan="4">Teknik Biomedika</th>
                             <th colspan="4">Kesehatan Tradisional</th>
                             <th colspan="4">Nakes lainnya</th>
                             <th colspan="4">Keperawatan</th>
                             <th colspan="4">Kebidanan</th>
                             <th colspan="4">Kefarmasian</th>
                             <th colspan="4">Teknik Biomedika</th>
                             <th colspan="4">Kesehatan Lingkungan</th>
                             <th colspan="4">Gizi</th>
                             <th colspan="4">Keteknisian Medis</th>
                             <th colspan="4">Struktural</th>
                             <th colspan="4">Dukungan Manajemen</th>
                             <th colspan="4">Pendidikan dan Pelatihan</th>
                             </tr>
                           <tr>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             <th colspan="2">PNS</th>
                             <th colspan="2">Non PNS</th>
                             </tr>
                           <tr>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                             <th>L</th>
                             <th>P</th>
                           </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="9" style="text-align:center"> No Data to Display</td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                </div>
            </div>
            </div>  
        </div>
        </div>
    </section>
  </div>

<?php $this->load->view('themes/footer');?>
<!-- MODAL -->
<div class="modal fade" id="defaultModal" role="dialog">
  <div class="modal-dialog modal-m" role="document">
    <div class="modal-content">
    <div class="modal-header">
      <button class="pull-right btn bg-red" data-dismiss="modal"><i class="fa fa-close"></i></button>
      <h4 class="modal-title" id="defaultModalLabel">Tambah</h4>
    </div>
      
    <div class="modal-body form">
      <?php echo form_open('#',array('id' => 'formmodal','class'=> 'form-horizontal', 'enctype' => 'multipart/form-data'))?>

        <div class="form-group">
          <label class="col-md-3 control-label">File Excel </label>
          <div class="col-md-9">
              <input type="file" id="file_excel" name="file_excel" multiple="" accept=".xls , .xlsx">
              <p class="help-block"> gunakan file dengan format .xls atau .xlsx</p>
          </div>
        </div>

      <?php echo form_close()?>
    </div>
      <div class="modal-footer">
      <button type="button" id="btn_simpan" class="btn bg-blue waves-effect" onclick="import_excel()" title="Isi Semua Untuk Menyimpan">Simpan</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var table;

  $(document).ready(function(){
    $('.select2').select2();
    $('[data-mask]').inputmask();

    table = $('#table').DataTable({ 
        "processing"    : true,
        "serverSide"    : true,
        "scrollX"       : true,
        "iDisplayLength": 25,
        "paging"        : true,
        "lengthChange"  : false,
        "searching"     : false,
        "ordering"      : false,
        "info"          : false,
        "autoWidth"     : true,
        "order"         : [],
        "lengthMenu"    : [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        "columnDefs"    : [{ "searchable": true, "targets": 0, }],
        "fixedColumns"  : {"leftColumns": 2 },
        "ajax": {
            "url"   : ci_baseurl + "administration/sdmk_list/ajax_list",
            "type"  : "POST",
            "data"  : function(d){
            }
        },
      });

      var table = $('#table').DataTable();
      table.columns().every( function () {
          var that = this;
          $( 'cari', this.footer() ).on( 'keyup change', function () {
              if (that.search() !== this.value) {
                  that.search(this.value ).draw();
              }
          } );
      } );

  });

  function modalload() {
    $('#btn_simpan').show();
    $('#formmodal').trigger("reset");
    $('#defaultModal').modal('show'); 
    $('#title_method').text('Update Data SDMK');
  }

  function refresh(){
    var table = $('#table').DataTable( );
    table.ajax.reload(null,false);
  }

  function import_excel() {
    if($('#file_excel').get(0).files.length === 0){
      swal("Silahkan Pilih File Excel");
    }else{
      swal({
          title               : "Simpan Data",
          text                : "Yakin Simpan Data Berikut ?",
          type                : "warning",
          showCancelButton    : true,
          confirmButtonColor  : "#DD6B55",
          confirmButtonText   : "Ya, Simpan",
          cancelButtonText    : 'Batal',
          closeOnConfirm      : false,
          showLoaderOnConfirm : true,
      },
      function(){
        var formData = new FormData($('#formmodal')[0]);
          $.ajax({
          url     : ci_baseurl + "administration/sdmk_list/import_sdmk",
          type    : 'POST',
          dataType  : 'JSON',
          data    : formData,
          contentType : false,
          processData : false,
          success: function(data) {
            var ret = data.success;
            if(ret === true) {
              swal({
                title   : "Success!",
                text    : "Data Berhasil Disimpan",
                type    : "success",
                timer   : 1500,
              });
              refresh();
            } else {
              swal({
                title   : "Gagal!",
                html    : data.messages,
                type    : "warning",
                timer   : 1500,
              });
            }
          }
        });
      });
    } 
  }




</script>