<?php $this->load->view('themes/header');?>

    <section class="content">
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4>Telepon</h4>
              <h4 id="box_phone"><b>phone</b></h4>
            </div>
            <div class="icon">
              <i class="glyphicon glyphicon-phone-alt"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
             <h4>Email</h4>
              <h4 id="box_email"><b>mail</b></h4>
            </div>
            <div class="icon">
              <i class="glyphicon glyphicon-envelope"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h4>Website</h4>
              <h4 id="box_web"><b>web</b></h4>
            </div>
            <div class="icon">
              <i class="glyphicon glyphicon-globe"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h4>Status Akreditasi</h4>
              <h4 id="box_akre"><b>akre</b></h4>
            </div>
            <div class="icon">
              <i class="glyphicon glyphicon-book"></i>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-7">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">Profil</h4>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body" id="div_profil" name="div_profil" >
              <?php echo form_open('#',array('id' => 'form_profil','class'=> 'form-horizontal'))?>
                  <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $users->id_sarana?>" readonly>

                  <div class="form-group">
                    <label class="col-sm-4 control-label">Nama Fasyankes<span style="color: red"> *</span></label>
                    <div class="col-sm-8">
                    <select class="form-control" id="nama_fasyankes" name="nama_fasyankes">
                          <?php
                          $value = $this->Model_masterdata_pengguna->list_sarana();
                          foreach($value as $list){
                              echo "<option value='".$list->id_sarana."'>".$list->nama_sarana."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-4 control-label">Nama Direktur Utama/Kepala Puskesmas/ Penanggung Jawab Klink<span style="color: red"> *</span></label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="direktur_fasyankes" name="direktur_fasyankes" placeholder="Nama Direktur Utama/Kepala Puskesmas/ PenanggungJawab Klink">
                    </div>
                  </div>

                  <div class="form-group" id="div_jenis" hidden="">
                      <label class="col-sm-4 control-label">Jenis</label>
                      <div class="col-sm-8">
                      <select class="form-control" id="jenis_fasyankes" name="jenis_fasyankes">

                      </select>
                      </div>
                  </div>

                  <div class="form-group" id="div_kelasrs" hidden="">
                      <label class="col-sm-4 control-label">Kelas RS</label>
                      <div class="col-sm-8">
                      <select class="form-control" id="kelasrs_fasyankes" name="kelasrs_fasyankes">
                        <option value="A">Kelas A</option>
                        <option value="B">Kelas B</option>
                        <option value="C">Kelas C</option>
                        <option value="D">Kelas D</option>
                      </select>
                      </div>
                  </div>

                  <div class="form-group" id="div_kepemilikan">
                      <label class="col-sm-4 control-label">Kepemilikan</label>
                      <div class="col-sm-8">
                      <select class="form-control" id="kepemilikan_fasyankes" name="kepemilikan_fasyankes">
                        <option value="bumn">BUMN</option>
                        <option value="pempus">Pemerintah Pusat</option>
                        <option value="pemda">Pemerintah Daerah</option>
                        <option value="swasta">Swasta</option>
                        <option value="tni">TNI / Polri</option>
                      </select>
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-sm-4 control-label">Nama Penyelenggara</label>
                      <div class="col-sm-8">
                      <input type="text" class="form-control" id="penyelenggara_fasyankes" name="penyelenggara_fasyankes" placeholder="Nama Penyelenggara">
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-sm-4 control-label">Alamat</label>
                      <div class="col-sm-8">
                      <input type="text" class="form-control" id="alamat_fasyankes" name="alamat_fasyankes" placeholder="Alamat RS" rows='3'>
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-sm-4 control-label">Kab/ Kota</label>
                      <div class="col-sm-8">
                      <input type="text" class="form-control" id="kota_fasyankes" name="kota_fasyankes" placeholder="Kab/ Kota" value="Jakarta Utara">
                      </div>
                  </div>
                  
                  <div class="form-group">
                      <label class="col-sm-4 control-label">Luas Tanah</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                          <input type="number" class="form-control" id="luas_tanah_fasyankes" name="luas_tanah_fasyankes" placeholder="Luas Tanah">
                            <span class="input-group-addon">m<sup>2</sup></span>
                        </div>
                      
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-sm-4 control-label">Luas Bangunan</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                          <input type="number" class="form-control" id="luas_bangunan_fasyankes" name="luas_bangunan_fasyankes" placeholder="Luas Bangunan">
                            <span class="input-group-addon">m<sup>2</sup></span>
                        </div>
                      
                      </div>
                  </div>
                  
                  <div class="form-group">
                      <label class="col-sm-4 control-label">No. Telpon (call center)</label>
                      <div class="col-sm-8">
                      <input type="text" class="form-control" id="telp_fasyankes" name="telp_fasyankes" placeholder="No. Telpon (call center)">
                      </div>
                  </div>
                  
                  <div class="form-group">
                      <label class="col-sm-4 control-label">Alamat email</label>
                      <div class="col-sm-8">
                      <input type="email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="form-control" id="email_fasyankes" name="email_fasyankes" placeholder="Alamat email">
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-sm-4 control-label">Alamat Website</label>
                      <div class="col-sm-8">
                      <input type="text" class="form-control" id="web_fasyankes" name="web_fasyankes" placeholder="Alamat Website">
                      </div>
                  </div>
                  
              <?php echo form_close()?> 
            </div>
            <div class="box-footer">
              <button name="btn_simpan" class="btn btn-primary pull-right" onclick="printDiv('div_profil')"><i class="fa fa-save"></i>&nbsp; Print</button>
            </div>
          </div>
        </div>

        <div class="col-md-5">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Perizinan dan Akreditasi</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <div class="box-body" id="div_perizinan" name="div_perizinan" >
            <?php echo form_open('#',array('id' => 'form_perizinan','class'=> 'form-horizontal'))?>
                <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $users->id_sarana?>" readonly>

                <div class="form-group">
                  <label class="col-sm-4 control-label">No Surat Perizinan<span style="color: red"> *</span></label>
                  <div class="col-sm-8">
                  <input type="text" class="form-control" id="no_surat_izin" name="no_surat_izin" placeholder="No Surat Perizinan">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label">Masa Berlaku Izin<span style="color: red"> *</span></label>
                  <div class="col-sm-8">
                      <div class="input-group">
                        <input type="text" class="form-control datepicker" id="masa_berlaku_izin" name="masa_berlaku_izin" placeholder="Masa Berlaku Izin" readonly="">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Status Akreditasi<span style="color: red"> *</span></label>
                    <div class="col-sm-8">
                    <select class="form-control" id="status_akre" name="status_akre">
                      <option value="dasar">Dasar</option>
                      <option value="madya">Madya</option>
                      <option value="utama">Utama</option>
                      <option value="paripurna">Paripurna</option>
                    </select>
                    </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label">No Sertifikat Akreditasi<span style="color: red"> *</span></label>
                  <div class="col-sm-8">
                  <input type="text" class="form-control" id="no_sertifikat_akre" name="no_sertifikat_akre" placeholder="No Sertifikat Akreditasi">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label">Masa Berlaku Akreditasi<span style="color: red"> *</span></label>
                  <div class="col-sm-8">
                      <div class="input-group">
                        <input type="text" class="form-control datepicker" id="masa_berlaku_akre" name="masa_berlaku_akre" placeholder="Masa Berlaku Akreditasi" readonly="">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                      </div>
                    </div>
                </div>
                
            <?php echo form_close()?>
            </div>
            <div class="box-footer">
              <button name="btn_simpan" class="btn btn-primary pull-right" onclick="printDiv('div_perizinan')"><i class="fa fa-save"></i>&nbsp; Print</button>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Tempat Tidur</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <div class="box-body" id="div_bed" name="div_bed">
              <div class="table-responsive m-t-40">
              <table id="table_bed" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                        <th style="width:15px;">No</th>
                        <th>Kelas</th>
                        <th>Jumlah</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td colspan="3" style="text-align:center"> No Data to Display</td>
                    </tr>
                  </tbody>
              </table>
              </div>  
            </div>
            <div class="box-footer">
              <button name="btn_simpan" class="btn btn-primary pull-right" onclick="printDiv('div_bed')"><i class="fa fa-save"></i>&nbsp; Print</button>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Layanan</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <div class="box-body" id="div_layanan" name="div_layanan" >
              <div class="table-responsive m-t-40">
              <table id="table" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                        <th style="width:15px;">No</th>
                        <th>Pelayanan</th>
                        <th >Hapus</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td colspan="3" style="text-align:center"> No Data to Display</td>
                    </tr>
                  </tbody>
              </table>
              </div>
            </div>
            <div class="box-footer">
              <button name="btn_simpan" class="btn btn-primary pull-right" onclick="printDiv('div_layanan')"><i class="fa fa-save"></i>&nbsp; Print</button>
            </div>
          </div>
        </div>

      </div>       
    </section>
  </div>

<?php $this->load->view('themes/footer');?>

<script type="text/javascript">  
  $(document).ready(function(){
    get_data_profil();
    get_data_perizinan();
    $("#form_profil :input").prop("disabled", true);
    $("#form_perizinan :input").prop("disabled", true);

    table = $('#table').DataTable({ 
          "processing"    : true,
          "serverSide"    : true,
          "scrollX"       : false,
          "iDisplayLength": 25,
          "paging"        : true,
          "lengthChange"  : false,
          "searching"     : false,
          "ordering"      : false,
          "info"          : false,
          "autoWidth"     : true,
          "order"         : [],
          "lengthMenu"    : [[10, 20, 50, -1], [10, 20, 50, "All"]],
          "ajax": {
              "url"   : ci_baseurl + "administration/profil_sarana/ajax_list_layanan",
              "type"  : "POST",
              "data"  : function(d){
              }
          },
      });

      table_bed = $('#table_bed').DataTable({ 
          "processing"    : true,
          "serverSide"    : true,
          "scrollX"       : false,
          "paging"        : false,
          "lengthChange"  : false,
          "searching"     : false,
          "ordering"      : false,
          "info"          : false,
          "autoWidth"     : true,
          "ajax": {
              "url"   : ci_baseurl + "administration/profil_sarana/ajax_list_bed",
              "type"  : "POST",
              "data"  : function(d){
              }
          },
      });

      // $('#table_bed').find('input, textarea, button, select').attr('disabled','disabled');
      $("#div_bed :input").prop("disabled", true);
  });

  function get_data_profil() {
    id = $('#id').val();

    if(id !==''){ 
      $.ajax({
      url         : ci_baseurl + "administration/profil_sarana/ajax_get_profil",
      type        : 'POST',
      dataType    : 'JSON',
      data        : {id:id},
        success: function(data) {
          var ret = data.success;
          if(ret === true) {
            $('#nama_fasyankes').val(data.data.idsarana);
            $('#direktur_fasyankes').val(data.data.direktur);

            $('#jenis_fasyankes').find('option').remove();

            if (data.data.jenis_sarana == 'KLINIK') {
              $('#div_jenis').show();
              $('#jenis_fasyankes').val(data.data.jenis);

              $("#jenis_fasyankes").append('<option value="kpr">Klinik Pratama</option>');
              $("#jenis_fasyankes").append('<option value="kut">Klinik Utama</option>');
            }else if(data.data.jenis_sarana == 'RS'){
              $('#div_jenis').show();
              $('#div_kelasrs').show();

              $("#jenis_fasyankes").append('<option value="rsu">Rumah Sakit Umum</option>');
              $("#jenis_fasyankes").append('<option value="rsk">Rumah Sakit Khusus</option>');              

              $('#jenis_fasyankes').val(data.data.jenis);
              $('#kelasrs_fasyankes').val(data.data.kelas);
            }else if(data.data.jenis_sarana == 'PUSKES'){
              $('#div_kepemilikan').hide();
            }
            
            $('#kepemilikan_fasyankes').val(data.data.kepemilikan);
            $('#penyelenggara_fasyankes').val(data.data.penyelenggara);
            $('#alamat_fasyankes').val(data.data.alamat);
            $('#kota_fasyankes').val(data.data.kota);
            $('#luas_tanah_fasyankes').val(data.data.luas_tanah);
            $('#luas_bangunan_fasyankes').val(data.data.luas_bangunan);
            $('#telp_fasyankes').val(data.data.telp);
            $('#email_fasyankes').val(data.data.email);
            $('#web_fasyankes').val(data.data.web);


            $('#box_phone').html(data.data.telp);
            $('#box_email').html(data.data.email);
            $('#box_web').html(data.data.web);

          } else {
            swal('ID Data Tidak Ditemukan');
          }
        }
      });
    }else{
      swal('ID Data Tidak Ditemukan');
    }
  }

  function get_data_perizinan() {
    id = $('#id').val();

      if(id !==''){ 
        $.ajax({
        url         : ci_baseurl + "administration/profil_sarana/ajax_get_perizinan",
        type        : 'POST',
        dataType    : 'JSON',
        data        : {id:id},
          success: function(data) {
            var ret = data.success;
            if(ret === true) {
              $('#no_surat_izin').val(data.data.no_surat_izin);
              $('#masa_berlaku_izin').val(data.data.masa_berlaku_izin);
              $('#status_akre').val(data.data.status_akre);
              $('#no_sertifikat_akre').val(data.data.no_sertifikat_akre);
              $('#masa_berlaku_akre').val(data.data.masa_berlaku_akre);

              $('#box_akre').html(data.data.status_akre);
            } else {
              swal('ID Data Tidak Ditemukan');
            }
          }
        });
      }else{
        swal('ID Data Tidak Ditemukan');
      }
  }

  function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }
</script>