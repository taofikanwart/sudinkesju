<?php $this->load->view('themes/header');?>

    <section class="content">
        <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
            <div class="box-header with-border">
              <button type="button" id="btn_tambah" name="btn_tambah" class="btn btn-primary" onclick="tambah()"><i class="fa fa-plus"></i> Tambah</button>
              <button type="button" id="btn_refresh" name="btn_refresh" class="btn btn-info" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>
            </div>
                <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive m-t-40">
                    <table id="table" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Sarana</th>
                            <th>Status</th>
                            <th>Aktivitas Terakhir</th>
                            <th style="width:15px;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="9" style="text-align:center"> No Data to Display</td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                </div>
            </div>
            </div>  
        </div>
        </div>
    </section>
  </div>

<?php $this->load->view('themes/footer');?>
<!-- MODAL -->
<div class="modal fade" id="defaultModal" role="dialog">
  <div class="modal-dialog modal-m" role="document">
    <div class="modal-content">
    <div class="modal-header">
      <button class="pull-right btn bg-red" data-dismiss="modal"><i class="fa fa-close"></i></button>
      <h4 class="modal-title" id="defaultModalLabel">Tambah</h4>
    </div>
      
    <div class="modal-body form">
      <?php echo form_open('#',array('id' => 'formmodal','class'=> 'form-horizontal'))?>
        <input type="hidden" class="form-control" id="id" name="id" placeholder="ID Barang Otomatis" readonly>

        <div class="form-group">
          <label class="col-sm-4 control-label">Nama Lengkap<span style="color: red"> *</span></label>
          <div class="col-sm-8">
          <input type="text" class="form-control" id="namalengkap" name="namalengkap" placeholder="Nama Lengkap" onkeyup="cek()">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-4 control-label">Username<span style="color: red"> *</span></label>
          <div class="col-sm-8">
          <input type="text" class="form-control" id="username" name="username" placeholder="Username" onkeyup="cek()">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-4 control-label">Email<span style="color: red"> *</span></label>
          <div class="col-sm-8">
          <input type="text" class="form-control" id="email" name="email" placeholder="Email" onkeyup="cek()">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-4 control-label">Password<span style="color: red"> *</span></label>
          <div class="col-sm-8">
          <input type="text" class="form-control" id="passwd" name="passwd" placeholder="Password" onkeyup="cek()">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-4 control-label">Ulangi Password<span style="color: red"> *</span></label>
          <div class="col-sm-8">
          <input type="text" class="form-control" id="repasswd" name="repasswd" placeholder="Ulangi Password" onkeyup="cek()">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-4 control-label">Sarana</label>
          <div class="col-sm-8">
          <select class="form-control select2" style="width: 100%;" id="sarana" name="sarana" onchange="cek()">
            <option selected disabled></option>
              <?php
              $value = $this->Model_masterdata_pengguna->list_sarana();
              foreach($value as $list){
                  echo "<option value='".$list->id_sarana."'>".$list->nama_sarana."</option>";
              }
            ?>
          </select>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-4 control-label">Level Akses</label>
          <div class="col-sm-8">
          <select class="form-control" style="width: 100%;" id="levelakses" name="levelakses" onchange="cek()">
              <?php
              $value = $this->Model_masterdata_pengguna->list_group();
              foreach($value as $list){
                  echo "<option value='".$list->id."'>".$list->definition."</option>";
              }
            ?>
          </select>
          </div>
        </div>

      <?php echo form_close()?>
    </div>
      <div class="modal-footer">
      <button type="button" id="btn_simpan" class="btn bg-blue waves-effect" onclick="proses()" title="Isi Semua Untuk Menyimpan">Simpan</button>
      <button type="reset" id="btn_reset" class="btn bg-amber waves-effect" onclick="resetmodal()">Reset</button>
      <button type="button" id="btn_batal" class="btn bg-red waves-effect" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var prosesmethod; 
  var table;

  $(document).ready(function(){
    $('.select2').select2();
    $('[data-mask]').inputmask();

    table = $('#table').DataTable({ 
        "processing"    : true,
        "serverSide"    : true,
        "scrollX"       : true,
        "iDisplayLength": 25,
        "paging"        : true,
        "lengthChange"  : true,
        "searching"     : true,
        "ordering"      : true,
        "info"          : true,
        "autoWidth"     : true,
        "order"         : [],
        "lengthMenu"    : [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        "columnDefs"  : [{ "searchable": true, "targets": 0, }],
        "ajax": {
            "url"   : ci_baseurl + "administration/masterdata_pengguna/ajax_list",
            "type"  : "POST",
            "data"  : function(d){
            }
        },
        dom: 'Bfrtip',buttons: [
          {extend: 'print',exportOptions: {columns: [ 1,2,3,4 ]}}, 
          {extend: 'excel',exportOptions: {columns: [ 1,2,3,4 ]}}, 
          {extend: 'pdf'  ,exportOptions: {columns: [ 1,2,3,4 ]}}, 
          'pageLength'],
      });

      var table = $('#table').DataTable();
      table.columns().every( function () {
          var that = this;
          $( 'cari', this.footer() ).on( 'keyup change', function () {
              if (that.search() !== this.value) {
                  that.search(this.value ).draw();
              }
          } );
      } );

      $('.select2').select2({
        placeholder: 'Silahkan Pilih',
      });

      reset();
  });

  function modalload() {
    $('#btn_simpan').show();
    $('#btn_reset').show();
    $('#formmodal').trigger("reset");
    $('#defaultModal').modal('show'); 
  }

  function reset() {
    prosesmethod = "tambah";
    $('#formmodal').trigger("reset");
    $('#btn_simpan').prop("disabled", true);
    $('#title_method').text('Tambah Data');
  }
  function refresh(){
    var table = $('#table').DataTable( );
    table.ajax.reload(null,false);
  }

  function cek() {
    namalengkap  = $('#namalengkap').val();
    username     = $('#username').val();
    email        = $('#email').val();
    passwd       = $('#passwd').val();
    repasswd     = $('#repasswd').val();
    sarana       = $('#sarana').val();

    if(namalengkap == "" || username == "" || email == "" || passwd.length < 6 || passwd != repasswd || sarana == ""){
      $('#btn_simpan').prop("disabled", true);
    }else{
      $('#btn_simpan').prop("disabled", false);
    }
  }


  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  function tambah() {
    $('#title_method').text('Tambah Data');
    prosesmethod = "tambah";
    reset();
    modalload();
  }

  function edit(id) {
    $(window).scrollTop(0);
    reset();
    prosesmethod = "edit";
    $('#title_method').text('Sunting Data');

    if(id !==''){ 
      $.ajax({
      url         : ci_baseurl + "administration/masterdata_pengguna/ajax_get_by_id",
      type        : 'POST',
      dataType    : 'JSON',
      data        : {id:id},
        success: function(data) {
          var ret = data.success;
          if(ret === true) {
            modalload();
            $('#id').val(data.data.user_id);
            $('#username').val(data.data.username);
            $('#email').val(data.data.email);
            $('#namalengkap').val(data.data.nama_lengkap);
            $("#sarana").select2("val", data.data.id_sarana);
            $('#levelakses').val(data.data.group_id);

            cek();
          } else {
            swal('ID Data Tidak Ditemukan');
          }
        }
      });
    }else{
      swal('ID Data Tidak Ditemukan');
    }
  }

  function proses() {
    email        = $('#email').val();

    if (isEmail(email) == true) {
      if (prosesmethod == "tambah") {
        used_url  = ci_baseurl + "administration/masterdata_pengguna/ajax_add";
        title     = "Tambah Data";
        text      = "Data Ditambahkan, Pastikan Data Terinput Dengan Benar";
      }else if (prosesmethod == "edit") {
        used_url  = ci_baseurl + "administration/masterdata_pengguna/ajax_update"; 
        title     = "Sunting Data";
        text      = "Data Disunting, Pastikan Data Terinput Dengan Benar";
      }else{
        swal("Method Tidak Diketahui");
        return false;
      }

      swal({
        title   : title,
        text    : text,
        type    : "warning",
        showCancelButton  : true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText : "Ya, Tambah",
        closeOnConfirm    : false
        },
        function(){
        $.ajax({
          url     : used_url,
          type    : "POST",
          dataType: "JSON",
          data    : $('#formmodal').serialize(),
          success: function(data){
            var ret = data.status;
            if(ret === true) {
              swal({
                title   : "Success!",
                text    : "Data Berhasil Disimpan",
                type    : "success",
                timer   : 1500,
              });
              reset();
              refresh();
              $('#defaultModal').modal('hide');
            } else {
              swal("Data Gagal Ditambahkan");
            }
          },
          error: function (jqXHR, textStatus, errorThrown, data){
            swal("Gagal", "Data Gagal Disimpan", "error");
          }
        });    
      });
    }else{
      swal("input email dengan benar");
      exit();
    }
  }

  function remove(id) {
    swal({
      title   : "Yakin Akan Di Hapus?",
      text    : "Data yg sudah di hapus tidak bisa di kembalikan",
      type    : "warning",
      showCancelButton  : true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText : "Ya, Hapus",
      closeOnConfirm    : false
    },
    function(){
      $.ajax({
        url : ci_baseurl + "administration/masterdata_pengguna/ajax_delete/"+id,
        type: "POST",
        dataType: "JSON",
        success: function(data){
          refresh();
          swal({
            title   : "Success!",
            text    : "Data Berhasil Dihapus",
            type    : "success",
            timer   : 1500,
          });
        },
        error: function (jqXHR, textStatus, errorThrown){
          swal("Gagal", "Data Gagal Dihapus", "error");
          refresh();
        }
      });
    });
  }
</script>