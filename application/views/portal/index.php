<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?php echo $title; ?></title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/portal/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/portal/fonts/line-icons.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/portal/css/slicknav.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/portal/css/nivo-lightbox.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/portal/css/animate.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/portal/css/main.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/portal/css/responsive.css'); ?>">
  </head>
  <body>
    <header id="header-wrap">
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
        <div class="container">
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
            </button>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <a href="index.html" class="navbar-brand"><img src="<?php echo base_url('assets/portal/img/logo_sudin.png'); ?>" width="100%" alt=""></a>
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item active">
                <a class="nav-link" href="#header-wrap">
                  Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#services">
                  Pelayanan
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#blog">
                  Artikel
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#visimisi">
                  Visi&Misi
                </a>
              </li>              
              <li class="nav-item">
                <a class="nav-link" href="#gallery">
                  Gallery
                </a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link" href="#faq">
                  Faq
                </a>
              </li> -->
              <li class="nav-item">
                <a class="nav-link" href="#google-map-area">
                  Kontak
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('auth/login'); ?>">
                  Login
                </a>
              </li>

            </ul>
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="mobile-menu">  
          <li>
            <a class="page-scrool" href="#header-wrap">Home</a>
          </li>
          <li>
             <a class="page-scroll" href="#services">Pelayanan</a>
          </li>
          <li>
            <a class="page-scrool" href="#blog">Artikel Terbaru</a>
          </li>
          <li>
             <a class="page-scroll" href="#visimisi">Visi & Misi</a>
          </li>
          </li>
          <li>
            <a class="page-scroll" href="#gallery">Gallery</a>
          </li>
          <li>
            <a class="page-scroll" href="#google-map-area">Kontak Kami</a>
          </li>
          <li>
            <a class="page-scroll" href="<?php echo base_url('auth/login'); ?>">Login</a>
          </li>
        </ul>
        <!-- Mobile Menu End -->

      </nav>
      <!-- Navbar End -->


      <!-- Main Carousel Section Start -->
      <div id="main-slide" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#main-slide" data-slide-to="0" class="active"></li>
          <li data-target="#main-slide" data-slide-to="1"></li>
          <li data-target="#main-slide" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?php echo base_url('assets/portal/img/slider/slide02.jpg'); ?>" alt="First slide">
            <div class="carousel-caption d-md-block">
              <p class="fadeInUp wow" data-wow-delay=".6s">Selamat Datang di Website</p>
              <h1 class="wow bounceIn heading" data-wow-delay=".7s">Suku Dinas Kesehatan Jakarta Utara</h1>
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?php echo base_url('assets/portal/img/slider/slide01.jpg'); ?>" alt="Second slide">            
          </div>          
          <div class="carousel-item">
            <img class="d-block w-100" src="<?php echo base_url('assets/portal/img/slider/slide03.jpg'); ?>" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#main-slide" role="button" data-slide="prev">
          <span class="carousel-control" aria-hidden="true"><i class="lni-chevron-left"></i></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#main-slide" role="button" data-slide="next">
          <span class="carousel-control" aria-hidden="true"><i class="lni-chevron-right"></i></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

    </header>
    <!-- Header Area wrapper End -->

     <!-- Services Section Start -->
    <section id="services" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title-header text-center">
              <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Pelayanan</h1>
              <p class="wow fadeInDown" data-wow-delay="0.2s">Macam-macam Pelayanan Suku Dinas Kesehatan Jakarta Utara</p>
            </div>
          </div>
        </div>
        <div class="row services-wrapper">
          <!-- Services item -->
          <div class="col-md-6 col-lg-4 col-xs-12 padding-none">
            <div class="services-item wow fadeInDown" data-wow-delay="0.2s">
              <div class="icon">
                <i class="lni-user"></i>
              </div>
              <div class="services-content">
                <h3><a href="https://pelayanan.jakarta.go.id/">PTSP</a></h3>
                <p>Pelayanan Terpadu Satu Pintu</p>
              </div>
            </div>
          </div>
          <!-- Services item -->
          <div class="col-md-6 col-lg-4 col-xs-12 padding-none">
            <div class="services-item wow fadeInDown" data-wow-delay="0.4s">
              <div class="icon">
                <i class="lni-graph"></i>
              </div>
              <div class="services-content">
                <h3><a href="http://data.jakarta.go.id/group/kesehatan">Data Kesehatan</a></h3>
                <p>Open Data Kesehatan DKI Jakarta</p>
              </div>
            </div>
          </div>
          <!-- Services item -->
          <div class="col-md-6 col-lg-4 col-xs-12 padding-none">
            <div class="services-item wow fadeInDown" data-wow-delay="0.6s">
              <div class="icon">
                <i class="lni-home"></i>
              </div>
              <div class="services-content">
                <h3><a href="http://eis.dinkes.jakarta.go.id/dashboard.php">EIS Dinkes DKI</a></h3>
                <p>Informasi Ketersediaan Tempat Tidur Rawat Inap</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Services Section End -->
    

    <!-- About Section Start -->
    <section id="blog" class="sectionspons-padding">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title-header text-center">
              <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Artikel Terbaru</h1>
              <p class="wow fadeInDown" data-wow-delay="0.2s">Artikel Kegiatan-kegiatan Suku Dinas Kesehatan Jakarta Utara</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="about-item">
              <img class="img-fluid" src="<?php echo base_url('assets/portal/img/about/img1.jpg'); ?>" alt="">
              <div class="about-text">
                <h3><a href="#">Wanna Know Our Mission?</a></h3>
                <p>Lorem ipsum dolor sit amet, consectetuer commodo ligula eget dolor.</p>
                <a class="btn btn-common btn-rm" href="#">Read More</a>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="about-item">
              <img class="img-fluid" src="<?php echo base_url('assets/portal/img/about/img2.jpg'); ?>" alt="">
              <div class="about-text">
                <h3><a href="#">What you will learn?</a></h3>
                <p>Lorem ipsum dolor sit amet, consectetuer commodo ligula eget dolor.</p>
                <a class="btn btn-common btn-rm" href="#">Read More</a>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="about-item">
              <img class="img-fluid" src="<?php echo base_url('assets/portal/img/about/img3.jpg'); ?>" alt="">
              <div class="about-text">
                <h3><a href="#">What are the benifits?</a></h3>
                <p>Lorem ipsum dolor sit amet, consectetuer commodo ligula eget dolor.</p>
                <a class="btn btn-common btn-rm" href="#">Read More</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- About Section End -->

    <!-- Counter Area Start-->
    <!-- <section class="counter-section section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-3 col-xs-12 work-counter-widget text-center">
            <div class="counter wow fadeInRight" data-wow-delay="0.3s">
              <div class="icon"><i class="lni-map"></i></div>
              <p>Wst. Conference Center</p>
              <span>San Francisco, CA</span>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 col-xs-12 work-counter-widget text-center">
            <div class="counter wow fadeInRight" data-wow-delay="0.6s">
              <div class="icon"><i class="lni-timer"></i></div>
              <p>February 14 - 19, 2018</p>
              <span>09:00 AM – 05:00 PM</span>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 col-xs-12 work-counter-widget text-center">
            <div class="counter wow fadeInRight" data-wow-delay="0.9s">
              <div class="icon"><i class="lni-users"></i></div>
              <p>343 Available Seats</p>       
              <span>Hurryup! few tickets are left</span>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 col-xs-12 work-counter-widget text-center">
            <div class="counter wow fadeInRight" data-wow-delay="1.2s">
              <div class="icon"><i class="lni-coffee-cup"></i></div>
              <p>Free Lunch & Snacks</p>
              <span>Don’t miss it</span>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    <!-- Counter Area End-->

    <!-- Schedule Section Start -->
    <section id="visimisi" class="schedule section-padding">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title-header text-center">
              <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Visi dan Misi</h1>
              <p class="wow fadeInDown" data-wow-delay="0.2s">Suku Dinas Kesehatan<br> Jakarta Utara</p>
            </div>
          </div>
        </div>
        <div class="schedule-area row wow fadeInDown" data-wow-delay="0.3s">
          <div class="schedule-tab-title col-md-3 col-lg-3 col-xs-12">
            <div class="table-responsive">
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="monday-tab" data-toggle="tab" href="#visi" role="tab" aria-controls="monday" aria-expanded="true">
                    <div class="item-text">
                      <h4>Visi & Misi</h4>
                    </div>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="tuesday-tab" data-toggle="tab" href="#struktur" role="tab" aria-controls="tuesday">
                    <div class="item-text">
                      <h4>Struktur Organisasi</h4>
                    </div>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="wednesday-tab" data-toggle="tab" href="#tujuan" role="tab" aria-controls="wednesday">
                    <div class="item-text">
                      <h4>Tujuan dan Fungsi</h4>
                    </div>
                  </a>
                </li>
                
              </ul>
            </div>
          </div>
          <div class="schedule-tab-content col-md-9 col-lg-9 col-xs-12 clearfix">
            <div class="tab-content" id="myTabContent">

              <div class="tab-pane fade show active" id="visi" role="tabpanel" aria-labelledby="monday-tab">
                <div id="accordion">
                  <div class="card">
                    <div id="headingOne1">
                      <div class="collapsed card-header" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="false" aria-controls="collapseOne1">
                        <div class="images-box">
                          <!-- <img class="img-fluid" src="<?php echo base_url('assets/portal/img/speaker/speakers-1.jpg'); ?>" alt=""> -->
                        </div>                     
                        <span class="time"></span>
                        <h2>Visi</h2>
                        <h5 class="name"></h5>
                      </div>
                    </div>
                    <div id="collapseOne1" class="collapse show" aria-labelledby="headingOne1" data-parent="#accordion2">
                      <div class="card-body">
                        <p>Menjadi Suku DInas Kesehatan yang Profesional dan Terstandarisasi Menuju "Jakarta Utara Sehat, Maju, Lestari dan Berbudaya"
                        </p>
                      </div>
                    </div>
                    <div class="card">
                    <div id="headingTwo2">
                      <div class="collapsed card-header" data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                        <div class="images-box">
                          <!-- <img class="img-fluid" src="<?php echo base_url('assets/portal/img/speaker/speakers-2.jpg'); ?>" alt=""> -->
                        </div>                     
                        <span class="time"></span>
                        <h2>Misi</h2>
                        <h5 class="name"></h5>
                      </div>
                    </div>
                    <div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo2" data-parent="#accordion2">
                      <div class="card-body">
                        <p>1. Meningkatkan kompetensi sumber daya manusia Suku Dinas Kesehatan Kota Administrasi Jakarta Utara.<br>
                            2. Meningkatkan sistem pengolahan data dan informasi terintegrasi.<br>
                            3. Membudayakan gerakan masyarakat hidup sehat.<br>
                            4. Meningkatkan kualitas pembinaan, pengawasan dan pengendalian sumber daya manusia, sarana dan prasarana kesehatan. <br>
                            5. Meningkatkan sinergitas lintas program, koordinasi intas sektor serta siap, tanggap, galang dalam upaya pencegahan dan penaggulangan permasalahan kesehatan.</p>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>
              </div>

              <div class="tab-pane fade" id="struktur" role="tabpanel" aria-labelledby="tuesday-tab">
                <div id="accordion">
                  <div class="card">
                    <div id="headingOne1">
                      <div class="collapsed card-header" data-toggle="collapse" data-target="#struktur1" aria-expanded="false" aria-controls="collapseOne1">
                        <div class="images-box">
                          <!-- <img class="img-fluid" src="<?php echo base_url('assets/portal/img/speaker/speakers-1.jpg'); ?>" alt=""> -->
                        </div>                     
                        <span class="time"></span>
                        <h2>Struktur</h2>
                        <h5 class="name"></h5>
                      </div>
                    </div>
                    <div id="struktur1" class="collapse show" aria-labelledby="headingOne1" data-parent="#accordion2">
                      <div class="card-body">
                        <p>Gambar Struktur Organisasi
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="tab-pane fade" id="tujuan" role="tabpanel" aria-labelledby="wednesday-tab">
                <div id="accordion">
                  <div class="card">
                    <div id="headingOne1">
                      <div class="collapsed card-header" data-toggle="collapse" data-target="#tujuan1" aria-expanded="false" aria-controls="collapseOne1">
                        <div class="images-box">
                          <!-- <img class="img-fluid" src="<?php echo base_url('assets/portal/img/speaker/speakers-1.jpg'); ?>" alt=""> -->
                        </div>                     
                        <span class="time"></span>
                        <h2>Tujuan</h2>
                        <h5 class="name"></h5>
                      </div>
                    </div>
                    <div id="tujuan1" class="collapse show" aria-labelledby="headingOne1" data-parent="#accordion2">
                      <div class="card-body">
                        <p>1. Tujuan
                        </p>
                      </div>
                    </div>
                    <div class="card">
                    <div id="headingTwo2">
                      <div class="collapsed card-header" data-toggle="collapse" data-target="#tujuan2" aria-expanded="false" aria-controls="collapseTwo2">
                        <div class="images-box">
                          <!-- <img class="img-fluid" src="<?php echo base_url('assets/portal/img/speaker/speakers-2.jpg'); ?>" alt=""> -->
                        </div>                     
                        <span class="time"></span>
                        <h2>Fungsi</h2>
                        <h5 class="name"></h5>
                      </div>
                    </div>
                    <div id="tujuan2" class="collapse" aria-labelledby="headingTwo2" data-parent="#accordion2">
                      <div class="card-body">
                        <p>1. Fungsi</p>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Schedule Section End -->

    <!-- Gallary Section Start -->
    <section id="gallery" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title-header text-center">
              <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Gallery</h1>
              <p class="wow fadeInDown" data-wow-delay="0.2s">Foto-foto kegiatan</p>
            </div>
          </div> 
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-lg-4">
            <div class="gallery-box">
              <div class="img-thumb">
                <img class="img-fluid" src="<?php echo base_url('assets/portal/img/gallery/img-1.jpg'); ?>" alt="">
              </div>
              <div class="overlay-box text-center">
                <a class="lightbox" href="<?php echo base_url('assets/portal/img/gallery/img-1.jpg'); ?>'); ?>">
                  <i class="lni-plus"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="ccol-md-6 col-sm-6 col-lg-4">
            <div class="gallery-box">
              <div class="img-thumb">
                <img class="img-fluid" src="<?php echo base_url('assets/portal/img/gallery/img-2.jpg'); ?>" alt="">
              </div>
              <div class="overlay-box text-center">
                <a class="lightbox" href="<?php echo base_url('assets/portal/img/gallery/img-2.jpg'); ?>">
                  <i class="lni-plus"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="ccol-md-6 col-sm-6 col-lg-4">
            <div class="gallery-box">
              <div class="img-thumb">
                <img class="img-fluid" src="<?php echo base_url('assets/portal/img/gallery/img-3.jpg'); ?>" alt="">
              </div>
              <div class="overlay-box text-center">
                <a class="lightbox" href="<?php echo base_url('assets/portal/img/gallery/img-3.jpg'); ?>">
                  <i class="lni-plus"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="ccol-md-6 col-sm-6 col-lg-4">
            <div class="gallery-box">
              <div class="img-thumb">
                <img class="img-fluid" src="<?php echo base_url('assets/portal/img/gallery/img-4.jpg'); ?>" alt="">
              </div>
              <div class="overlay-box text-center">
                <a class="lightbox" href="<?php echo base_url('assets/portal/img/gallery/img-4.jpg'); ?>">
                  <i class="lni-plus"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="ccol-md-6 col-sm-6 col-lg-4">
            <div class="gallery-box">
              <div class="img-thumb">
                <img class="img-fluid" src="<?php echo base_url('assets/portal/img/gallery/img-5.jpg'); ?>" alt="">
              </div>
              <div class="overlay-box text-center">
                <a class="lightbox" href="<?php echo base_url('assets/portal/img/gallery/img-5.jpg'); ?>">
                  <i class="lni-plus"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="ccol-md-6 col-sm-6 col-lg-4">
            <div class="gallery-box">
              <div class="img-thumb">
                <img class="img-fluid" src="<?php echo base_url('assets/portal/img/gallery/img-6.jpg'); ?>" alt="">
              </div>
              <div class="overlay-box text-center">
                <a class="lightbox" href="<?php echo base_url('assets/portal/img/gallery/img-6.jpg'); ?>">
                  <i class="lni-plus"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="row justify-content-center mt-3">
          <div class="col-xs-12">
            <a href="#" class="btn btn-common">Browse All</a>
          </div>
        </div>
      </div>
    </section>
    <!-- Gallary Section End -->

    <!-- Sponsors Section Start -->
    <section id="sponsor" class="counter-section section-padding">
      <div class="container">
        <div class="row mb-30 text-center wow fadeInDown" data-wow-delay="0.3s">
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="<?php echo base_url('assets/portal/img/sponsors/logo-01.png'); ?>" alt=""></a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="<?php echo base_url('assets/portal/img/sponsors/logo-02.png'); ?>" alt=""></a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="<?php echo base_url('assets/portal/img/sponsors/logo-03.png'); ?>" alt=""></a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="<?php echo base_url('assets/portal/img/sponsors/logo-04.png'); ?>" alt=""></a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Sponsors Section End -->

    <!-- Map Section Start -->
    <section id="google-map-area">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div>
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15868.123020238896!2d106.8938234!3d-6.1265637!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x21db65526a18334e!2sSuku%20Dinas%20Kesehatan%20Jakarta%20Utara!5e0!3m2!1sid!2sid!4v1576506073195!5m2!1sid!2sid" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Map Section End -->

    <!-- Footer Section Start -->
    <footer class="footer-area section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
            <h3><img src="<?php echo base_url('assets/portal/img/logo_sudin.png'); ?>" width="100%" alt=""></h3>
            <p>
              Jl. Yos Sudarso No.27-29, RT.3/RW.14, Kebon Bawang, Kec. Tanjung Priok, Kota Jakarta Utara, Daerah Khusus Ibukota Jakarta 14320
            </p>
          </div>
          <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
            <h3>Link Terkait</h3>
            <ul>
              <li><a href="https://www.kemkes.go.id/">Kementerian Kesehatan</a></li>
              <li><a href="https://jakarta.go.id/">Pemprov DKI Jakarta</a></li>
              <li><a href="http://utara.jakarta.go.id/">Pemkot Jakarta Utara</a></li>
              <li><a href="https://dinkes.jakarta.go.id/">Dinas Kesehatan DKI</a></li>
              <li><a href="https://labkesda.jakarta.go.id/">Labkesda DKI</a></li>
              <li><a href="https://agddinkes.jakarta.go.id/">Ambulance Gawat Darurat DKI</a></li>
            </ul>
          </div>
          <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
            <h3>Artikel Terbaru</h3>
            <ul class="image-list">
              <li>
                <figure class="overlay">
                  <img class="img-fluid" src="<?php echo base_url('assets/portal/img/art/a1.jpg'); ?>" alt="">
                </figure>
                <div class="post-content">
                  <h6 class="post-title"> <a href="blog-single.html">Lorem ipsm dolor sumit.</a> </h6>
                  <div class="meta"><span class="date">October 12, 2018</span></div>
                </div>
              </li>
              <li>
                <figure class="overlay">
                  <img class="img-fluid" src="<?php echo base_url('assets/portal/img/art/a2.jpg'); ?>" alt="">
                </figure>
                <div class="post-content">
                  <h6 class="post-title"><a href="blog-single.html">Lorem ipsm dolor sumit.</a></h6>
                  <div class="meta"><span class="date">October 12, 2018</span></div>
                </div>
              </li>
            </ul>
          </div>
          <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.8s">
            
            <!-- /.widget -->
            <div class="widget">
              <h5 class="widget-title">FOLLOW US ON</h5>
              <ul class="footer-social">
                <li><a class="facebook" href="#"><i class="lni-facebook-filled"></i></a></li>
                <li><a class="twitter" href="#"><i class="lni-twitter-filled"></i></a></li>
                <li><a class="instagram" href="#"><i class="lni-instagram-filled"></i></a></li>
                <li><a class="envelope" href="#"><i class="lni-envelope"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- Footer Section End -->

    <div id="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="site-info">
              <p>Copyright © 2019 | Designed and Developed by <a href="http://uideck.com" rel="nofollow">Suku Dinas Kesehatan Jakarta Utara</a></p>
            </div>      
          </div>
        </div>
      </div>
    </div>

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
    	<i class="lni-chevron-up"></i>
    </a>

    <div id="preloader">
      <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
      </div>
    </div>

    <!-- jQuery first, then Popper.js') ?>, then Bootstrap JS -->
    <script src="<?php echo base_url('assets/portal/js/jquery-min.js') ?>"></script>
    <script src="<?php echo base_url('assets/portal/js/popper.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/portal/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/portal/js/jquery.countdown.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/portal/js/jquery.nav.js') ?>"></script>
    <script src="<?php echo base_url('assets/portal/js/jquery.easing.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/portal/js/wow.js') ?>"></script>
    <script src="<?php echo base_url('assets/portal/js/jquery.slicknav.js') ?>"></script>
    <script src="<?php echo base_url('assets/portal/js/nivo-lightbox.js') ?>"></script>
    <script src="<?php echo base_url('assets/portal/js/main.js') ?>"></script>
    <script src="<?php echo base_url('assets/portal/js/form-validator.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/portal/js/contact-form-script.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/portal/js/map.js') ?>"></script>
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      
  </body>
</html>
