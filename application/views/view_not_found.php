<?php $this->load->view('themes/header');?>
  <section class="content">

    <div class="error-page">
      <h2 class="headline text-red">404</h2>

      <div class="error-content">
        <h3><i class="fa fa-warning text-red"></i> Mohon Maaf</h3>

        <p>
          Alamat website yang anda tuju tidak diketahui,
          Silahkan <a href="<?php echo base_url('administration')?>">kembali ke Dashbaord</a>.
        </p>

        <!-- <form class="search-form">
          <div class="input-group">
            <input type="text" name="search" class="form-control" placeholder="Search">

            <div class="input-group-btn">
              <button type="submit" name="submit" class="btn btn-danger btn-flat"><i class="fa fa-search"></i>
              </button>
            </div>
          </div>
        </form> -->
      </div>
    </div>

  </section>
</div>

<?php $this->load->view('themes/footer');?>