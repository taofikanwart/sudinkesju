<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Puskesmas Pademangan</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link href="<?php echo base_url()?>assets/img/favicon.ico" rel="icon">
  <link href="<?php echo base_url()?>assets/img/favicon.ico" rel="apple-touch-icon">

  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bootstrap/css/bootstrap.min.css');?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/dist/css/AdminLTE.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/plugins/iCheck/square/blue.css');?>">
</head>
<body class="hold-transition login-page" style="background:#b2e0f9;">
<div class="login-box hidden-xs">
  <div class="login-logo">
    <a href="<?php echo base_url();?>"><b>Selamat Datang</b><br></a>
  </div>  
  <div class="login-box-body">
    <img width="100%" src="<?php echo base_url()?>assets/portal/img/logo_sudin.png" align="center"><br>
    <div class="login-box-body">    

      <?php
        $attributes = array('class' => 'validate-form');
        echo form_open('auth/login/do_login', $attributes);
      ?>
      <?php
        $messages = $this->session->flashdata('messages');
        if($messages != ''){
          if(is_array($messages))
            echo '<div class="alert alert-danger"><button class="close" data-dismiss="alert" type="button">×</button><span>'.$messages[0].'</span></div>';
          else
            echo '<div class="alert alert-danger"><button class="close" data-dismiss="alert" type="button">×</button><span>'.$messages.'</span></div>';
        }
      ?>
        <div class="form-group has-feedback">
          <input class="form-control" type="text" autocomplete="off" placeholder="Username / NIP" name="username" id="username" required>
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input class="form-control" type="password" autocomplete="off" placeholder="Password" name="password" id="password" required>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div>
        </div>
      </form>

    <div class="social-auth-links text-center">
      silahkan login terlebih dahulu<br>untuk menggunakan aplikasi ini

      
    </div>
  </div>
</div>
</body>
</html>

<script src="<?php echo base_url('assets/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/plugins/iCheck/icheck.min.js')?>"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });

  function daftar_fb() {
    alert("Mohon Maaf untuk sementara fitur belum tersedia");
  }
</script>