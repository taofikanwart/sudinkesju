<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] 	= array();
// $autoload['libraries'] 	= array();
$autoload['libraries'] 	= array('session','encryption','database','form_validation','parser','Aauth');
$autoload['drivers'] 	= array();
$autoload['helper'] 	= array('url','form','menutree','file','download');
$autoload['config'] 	= array();
$autoload['language'] 	= array();
$autoload['model'] 		= array('Menu_model');
