<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] 	= 'home/';
$route['404_override'] 			= 'not_found';
$route['translate_uri_dashes'] 	= FALSE;
