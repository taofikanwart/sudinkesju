###################
Website Sudinkes Jakatra Utara
###################

*******************
Kebutuhan Installasi
*******************
- Server Linux / Windows (disarankan Linux Ubuntu)
- Web Service Apache/Nginx
- PHP 7
- MySQL / MariaDB

*******************
Cara Installasi
*******************
- clone project di folder htdoc / html
- set database di file /application/config/database.php

'hostname' => 'localhost',
'username' => 'usernamenya',
'password' => 'passwordnya',
'database' => 'nama_databasenya',

ganti baris di atas sesuai user db di server

- set htaccess di file .htaccess

	# CodeIgniter Subfolder .Htaccess
	# today hints by www.insanen.com
	Options +FollowSymLinks

	RewriteEngine On
	# Please do-not forget to change RewriteBase /pnm
	RewriteBase /namafolder

	RewriteRule ^$ index.php [L]
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond $1 !^(index\.php|robots\.txt|favicon\.ico)
	RewriteRule 	^(.*)$ index.php?/$1 [L]

	#<IfModule mod_rewrite.c>
	#RewriteEngine On
	#RewriteBase /namafolder
	#RewriteCond %{REQUEST_FILENAME} !-f
	#RewriteCond %{REQUEST_FILENAME} !-d
	#RewriteRule . /index.php [L]
	#</IfModule>

ganti nama folder sesuai lokasi

**************************************
Taofik Anwar | taofikanwar@gmail.com
**************************************

*******************
Komponen
*******************
- Codeigniter 3.x.x
- Aauth
- Admin LTE
- Sweet Alert
- PHP Excel
- JSTree
- Menu Tree

php7.2-intl php7.2-xsl php7.2-gd php7.2-mbstring